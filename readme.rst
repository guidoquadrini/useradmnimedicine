###################
User Admin Medicine
###################
Dashboard for user self-management. Target: Prepaid Medicine.

*******************
Technologies
*******************
PHP 7, Codeigniter 3, Javascript, Bootstrap 5, HTML5/CSS3, SQL Server Driver

*******************
Server Requirements
*******************
PHP version 5.6 or newer is recommended.

It should work on 5.3.7 as well, but we strongly advise you NOT to run
such old versions of PHP, because of potential security and performance
issues, as well as missing features.

************
Installation
************
Unzip system.zip.
Local: Config your virtual host.
Internet: Just Upload the code.
No database provided. (Atention: SQL Server project)