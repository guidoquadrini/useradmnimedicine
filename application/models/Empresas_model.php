<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Empresas_model extends CI_Model {

    public function __construct() {
        parent::__construct();

        $this->id_empresa = $this->session->userdata('id_empresa');
    }
   

    public function obtenerDatosGenerales() {
        $query = $this->db->query(
            "SELECT 
            (select max(fecha_alta) from empresas_movimientos EM where empresas_movimientos.id_empresa = em.id_empresa) fecha_alta
            ,empresas.razon_social
            ,empresas.cuit
            ,empresas.fecha_inicio_actividad
            ,empresas.id_tipo_sociedad
            --,tipos_sociedades.desc_tipo_sociedad
            ,empresas.id_rubro
            --,rubros.desc_rubro
            ,empresas.cuenta
        FROM empresas
            --LEFT JOIN tipos_sociedades ON tipos_sociedades.id_tipo_sociedad = empresas.id_tipo_sociedad
            LEFT JOIN tipos_empresas ON tipos_empresas.id_tipo_empresa = empresas.id_tipo_empresa
            --LEFT JOIN rubros ON rubros.id_rubro = empresas.id_rubro
            LEFT JOIN empresas_movimientos ON 
                empresas_movimientos.id_empresa_movimiento = (
                SELECT MAX(id_empresa_movimiento)
                FROM empresas_movimientos EMPMOV
                WHERE EMPMOV.id_empresa = empresas.id_empresa
            )
        WHERE empresas.id_empresa = $this->id_empresa " );

        $result = $query->result()[0];

        return $result;
    }

    public function actualizarDatosGenerales($razon_social,$fecha_inicio_actividad,$id_tipo_sociedad,$id_rubro,$cuenta){
        $fecha_inicio_actividad_value = $fecha_inicio_actividad == "" ? "NULL" : "'$fecha_inicio_actividad'";
         
         return $this->db->query("UPDATE empresas SET 
                razon_social = '$razon_social'
                ,fecha_inicio_actividad = $fecha_inicio_actividad_value
                ,id_tipo_sociedad = $id_tipo_sociedad
                ,id_rubro = $id_rubro
                ,cuenta = '$cuenta'
            WHERE id_empresa = $this->id_empresa "); 

    }

    public function obtenerDomicilios() {
        $query = $this->db->query(
            "SELECT
            --id
            domicilios.id_domicilio
            --datos del domilicilio legal/particular y postal, este recordset devuelve 2 registros
            ,domicilios.tipo_domicilio
            ,domicilios.calle
            ,domicilios.numero
            ,domicilios.piso
            ,domicilios.depto
            ,domicilios.referencia_adicional
            ,localidades.codigo_postal
            ,provincias.desc_provincia
            ,provincias.id_provincia
            ,localidades.desc_localidad
            ,localidades.id_localidad
            FROM domicilios
                LEFT JOIN localidades ON localidades.id_localidad = domicilios.id_localidad
                LEFT JOIN provincias ON provincias.id_provincia = localidades.id_provincia
                LEFT JOIN provincias provincias_as400 ON provincias_as400.id_provincia = domicilios.id_provincia_as400
                LEFT JOIN empresas ON empresas.id_dato_adicional = domicilios.id_dato_adicional
        WHERE empresas.id_empresa = $this->id_empresa");
        
        $result = $query->result(); //[0];

        return $result;
    }

    public function actualizarDomicilio($idDomicilio, $calle, $numero, $piso, $depto, $referenciaAdicional, $idLocalidad){
        
        return $this->db->query("UPDATE domicilios 
                                    SET 
                                        calle = '$calle'
                                        ,numero = '$numero'
                                        ,piso = '$piso'
                                        ,depto = '$depto'
                                        ,referencia_adicional = '$referenciaAdicional'
                                        ,id_localidad = $idLocalidad
                                        FROM empresas 
                                            inner join datos_adicionales  on empresas.id_dato_adicional = datos_adicionales.id_dato_adicional
                                            inner join domicilios on domicilios.id_dato_adicional = datos_adicionales.id_dato_adicional
                                        WHERE 
                                            id_empresa = $this->id_empresa AND id_domicilio = $idDomicilio"); 
    }

    public function obtenerDatosContacto() {
        $query = $this->db->query(
            "SELECT 
            datos_adicionales.telefono_fijo
            ,datos_adicionales.telefono_movil
            ,datos_adicionales.email1
            ,datos_adicionales.email2
            ,datos_adicionales.fax
        FROM datos_adicionales
            LEFT JOIN empresas ON empresas.id_dato_adicional = datos_adicionales.id_dato_adicional
        WHERE empresas.id_empresa = $this->id_empresa");
        
        $result = $query->result()[0];

        return $result;
    }   

    public function actualizarDatosContacto($telefonoFijo, $telefonoMovil, $email1, $email2, $fax){
        return $this->db->query(
                "UPDATE datos_adicionales SET 
                    datos_adicionales.telefono_fijo = '$telefonoFijo'
                    ,datos_adicionales.telefono_movil = '$telefonoMovil'
                    ,datos_adicionales.email1 = '$email1'
                    ,datos_adicionales.email2 = '$email2'
                    ,datos_adicionales.fax = '$fax'
                FROM empresas 
                    inner join datos_adicionales  on empresas.id_dato_adicional = datos_adicionales.id_dato_adicional
                WHERE 
                    id_empresa = $this->id_empresa");
        
    }

    
       
}