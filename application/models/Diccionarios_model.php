<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Diccionarios_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
   
    public function obtenerCargos(){
        $query = $this->db->query("SELECT id_cargo, desc_cargo FROM cargos");
        
        $result = $query->result();

        return $result;
    }

    public function obtenerLocalidades($idProvincia){
        $query = $this->db->query("SELECT id_localidad, desc_localidad, codigo_postal FROM localidades WHERE id_provincia = $idProvincia");
        
        $result = $query->result();

        return $result;
    }

    public function buscarLocalidades($idProvincia, $search, $limit){
        $query = $this->db->query("SELECT TOP $limit id_localidad, desc_localidad, codigo_postal 
            FROM localidades 
            WHERE id_provincia = $idProvincia AND desc_localidad LIKE '%$search%'");
        
        $result = $query->result();

        return $result;
    }

    public function buscarLocalidadesPorCodPostal($idProvincia, $search, $limit){
        $query = $this->db->query("SELECT TOP $limit id_localidad, desc_localidad, codigo_postal 
            FROM localidades 
            WHERE id_provincia = $idProvincia AND codigo_postal LIKE '$search%'");
        
        $result = $query->result();

        return $result;
    }

    public function obtenerProvincias(){
        $query = $this->db->query("SELECT id_provincia, desc_provincia FROM provincias");
        
        $result = $query->result();

        return $result;
    }

    public function obtenerBancos(){
        $query = $this->db->query("SELECT id_banco, desc_banco FROM bancos");
        
        $result = $query->result();

        return $result;
    }

    public function obtenerTiposDocumentos(){

        $query = $this->db->query("SELECT id_tipo_documento, desc_tipo_documento FROM tipos_documentos");
        
        $result = $query->result();

        return $result;
    }

    public function obtenerTiposDocumentosValidos(){

        $query = $this->db->query("SELECT id_tipo_documento, desc_tipo_documento FROM tipos_documentos WHERE id_tipo_documento != 0");
        
        $result = $query->result();

        return $result;
    }

    public function obtenerRubros(){

        $query = $this->db->query("SELECT id_rubro,desc_rubro FROM rubros");
        
        $result = $query->result();

        return $result;
    }

    public function obtenerTiposSociedades(){

        $query = $this->db->query("SELECT id_tipo_sociedad,desc_tipo_sociedad FROM tipos_sociedades");
        
        $result = $query->result();

        return $result;
    }

    public function obtenerTitulosAbreviados(){

        $query = $this->db->query("SELECT id_titulo_abreviado,desc_titulo_abreviado FROM titulos_abreviados");
        
        $result = $query->result();

        return $result;
    }
    
}