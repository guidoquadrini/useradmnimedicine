<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Prestadores_model extends CI_Model {

    public function __construct() {
        parent::__construct();

        $this->id_prestador = $this->session->userdata('id_prestador');
    }
   

    public function obtenerDatosGenerales() {
        $query = $this->db->query(
            "SELECT
            titulos_abreviados.desc_titulo_abreviado
            ,titulos_abreviados.id_titulo_abreviado
           ,prestadores.nombre_razon_social
           ,(select max(fecha_alta) from prestadores_movimientos PM where prestadores_movimientos.id_prestador = PM.id_prestador) fecha_alta
           ,tipos_documentos.desc_tipo_documento
           ,prestadores.nro_documento
           ,prestadores.fecha_nacimiento
           ,prestadores.cuil_cuit
           ,prestadores.inos_anssal
           ,prestadores.fecha_inicio_actividad
           ,prestadores.matricula_provincial
           ,prestadores.matricula_nacional
           ,prestadores.circulo
           ,prestadores_movimientos.fecha_desde_suspension
           ,prestadores_movimientos.fecha_hasta_suspension
           ,prestadores_motivos_suspensiones.desc_prestador_motivo_suspension
       FROM prestadores
           LEFT JOIN prestadores_movimientos ON 
               prestadores_movimientos.id_prestador_movimiento = (
                   SELECT MAX(id_prestador_movimiento) 
                   FROM prestadores_movimientos PRESTAMOV
                   WHERE PRESTAMOV.id_prestador = prestadores.id_prestador
               )
           LEFT JOIN tipos_documentos ON tipos_documentos.id_tipo_documento = prestadores.id_tipo_documento
           LEFT JOIN titulos_abreviados ON titulos_abreviados.id_titulo_abreviado = prestadores.id_titulo_abreviado
           LEFT JOIN prestadores_motivos_suspensiones on prestadores_movimientos.id_prestador_motivo_suspension = prestadores_motivos_suspensiones.id_prestador_motivo_suspension
       WHERE prestadores.id_prestador =  $this->id_prestador " );

        $result = $query->result()[0];

        return $result;
    }

    public function actualizarDatosPersonales($id_titulo_abreviado,$nombre_razon_social,$fecha_nacimiento,$inos_anssal,$fecha_inicio_actividad,$matricula_provincial,$matricula_nacional, $circulo){
        $circulo_value = (int)$circulo;
        $fecha_inicio_actividad_value = $fecha_inicio_actividad == "" ? "NULL" : "'$fecha_inicio_actividad'";
        $fecha_nacimiento_value = $fecha_nacimiento == "" ? "NULL" : "'$fecha_nacimiento'";
        
        return $this->db->query("UPDATE prestadores SET 
                id_titulo_abreviado = $id_titulo_abreviado
                ,nombre_razon_social = '$nombre_razon_social'
                ,fecha_nacimiento = $fecha_nacimiento_value
                ,inos_anssal = '$inos_anssal'
                ,fecha_inicio_actividad = $fecha_inicio_actividad_value
                ,matricula_provincial = '$matricula_provincial'
                ,matricula_nacional = '$matricula_nacional'
                ,circulo = $circulo_value
            WHERE id_prestador =$this->id_prestador "); 
            
    }

    public function obtenerDomicilios() {
        $query = $this->db->query(
            "SELECT
            --id
            domicilios.id_domicilio
            --datos del domilicilio legal/particular y postal, este recordset devuelve 2 registros
            ,domicilios.tipo_domicilio
            ,domicilios.calle
            ,domicilios.numero
            ,domicilios.piso
            ,domicilios.depto
            ,domicilios.referencia_adicional
            ,localidades.codigo_postal
            ,localidades.id_provincia
            ,provincias.desc_provincia
            ,localidades.id_localidad
            ,localidades.desc_localidad
            -- datos del pie del formulario
            ,datos_adicionales.telefono_fijo
            ,datos_adicionales.telefono_movil
            ,datos_adicionales.email1
            ,datos_adicionales.email2
            ,datos_adicionales.fax
            FROM domicilios
                LEFT JOIN localidades ON localidades.id_localidad = domicilios.id_localidad
                LEFT JOIN provincias ON provincias.id_provincia = localidades.id_provincia
                LEFT JOIN provincias provincias_as400 ON provincias_as400.id_provincia = domicilios.id_provincia_as400
                LEFT JOIN prestadores ON prestadores.id_dato_adicional = domicilios.id_dato_adicional
                LEFT JOIN datos_adicionales on domicilios.id_dato_adicional = datos_adicionales.id_dato_adicional
        WHERE prestadores.id_prestador = $this->id_prestador");
        
        $result = $query->result()[0];

        return $result;
    }

    public function actualizarDomicilio($calle, $numero, $piso, $depto, $referenciaAdicional, $idLocalidad){
        
        return $this->db->query("UPDATE domicilios 
                                    SET 
                                        calle = '$calle'
                                        ,numero = '$numero'
                                        ,piso = '$piso'
                                        ,depto = '$depto'
                                        ,referencia_adicional = '$referenciaAdicional'
                                        ,id_localidad = $idLocalidad
                                        FROM prestadores 
                                            inner join datos_adicionales  on prestadores.id_dato_adicional = datos_adicionales.id_dato_adicional
                                            inner join domicilios on domicilios.id_dato_adicional = datos_adicionales.id_dato_adicional
                                        WHERE 
                                            id_prestador = $this->id_prestador"); 
    }
 

    public function actualizarDatosContacto($telefonoFijo, $telefonoMovil, $email1, $email2, $fax){
        return $this->db->query(
                "UPDATE datos_adicionales SET 
                    datos_adicionales.telefono_fijo = '$telefonoFijo'
                    ,datos_adicionales.telefono_movil = '$telefonoMovil'
                    ,datos_adicionales.email1 = '$email1'
                    ,datos_adicionales.email2 = '$email2'
                    ,datos_adicionales.fax = '$fax'
                    FROM prestadores 
                        inner join datos_adicionales on prestadores.id_dato_adicional = datos_adicionales.id_dato_adicional
                    WHERE 
                        id_prestador = $this->id_prestador");
        
    }

    
       
}