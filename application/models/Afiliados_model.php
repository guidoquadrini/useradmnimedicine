<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Afiliados_model extends CI_Model {

    public function __construct() {
        parent::__construct();

        $this->id_afiliado = $this->session->userdata('id_afiliado');
    }
   

    public function obtenerDatosPersonales() {
        $query = $this->db->query(
            "SELECT
                --id
                afiliados.id_afiliado
                --datos de la cabecera
                ,grupos_familiares.nro_grupo_afiliado nro_afiliado
                ,CONCAT(afiliados.nombre_completo, ' (', grupos_familiares.nro_grupo_afiliado, '-', right ('00' + convert(varchar,afiliados.digito_parentesco),2) + ')' ) nro_nombre
                -- datos del formulario
                ,afiliados_estados.fecha_alta fecha_ingreso
                ,afiliados.nombre_completo apellido_nombre
                ,sexos.desc_sexo 
                ,tipos_documentos.desc_tipo_documento
                ,afiliados.nro_documento
                ,afiliados.cuil_cuit
                ,afiliados.fecha_nacimiento
                ,CASE
                    WHEN dateadd(year, datediff (year, afiliados.fecha_nacimiento, getdate()), afiliados.fecha_nacimiento) > getdate()
                    THEN datediff (year, afiliados.fecha_nacimiento, getdate()) - 1
                    ELSE datediff (year, afiliados.fecha_nacimiento, getdate())
                END edad
                ,cargos.id_cargo
                ,paises.desc_nacionalidad
                ,estados_civiles.desc_estado_civil
                ,afiliados_configuraciones.tipo_grupo
                ,prestadoras.nombre_corto prestadora_nombre_corto
                ,planes.nombre_corto plan_nombre_corto
                -- estado del afiliado
                ,afiliados_estados.id_estado
                ,afiliados_estados.desc_estado

            FROM afiliados
            LEFT JOIN sexos on sexos.id_sexo = afiliados.id_sexo
            LEFT JOIN afiliados_configuraciones ON afiliados_configuraciones.id_afiliado = afiliados.id_afiliado
            LEFT JOIN grupos_familiares_afiliados ON grupos_familiares_afiliados.id_afiliado = afiliados.id_afiliado AND grupos_familiares_afiliados.fecha_hasta is NULL
            LEFT JOIN grupos_familiares ON grupos_familiares.id_grupo_familiar = grupos_familiares_afiliados.id_grupo_familiar
            LEFT JOIN tipos_documentos ON afiliados.id_tipo_documento = tipos_documentos.id_tipo_documento
            LEFT JOIN cargos ON afiliados.id_cargo = cargos.id_cargo
            LEFT JOIN paises ON afiliados.id_pais = paises.id_pais
            LEFT JOIN estados_civiles ON afiliados.id_estado_civil = estados_civiles.id_estado_civil
            LEFT JOIN afiliados_planes ON
                grupos_familiares_afiliados.id_grupo_familiar = afiliados_planes.id_grupo_familiar AND
                afiliados_planes.id_afiliado_plan = (
                    SELECT MAX(id_afiliado_plan)
                    FROM afiliados_planes AP
                    WHERE AP.id_grupo_familiar = afiliados_planes.id_grupo_familiar
                )
            LEFT JOIN prestadoras_planes ON prestadoras_planes.id_prestadora_plan = afiliados_planes.id_prestadora_plan
            LEFT JOIN prestadoras ON prestadoras.id_prestadora = prestadoras_planes.id_prestadora
            LEFT JOIN planes on planes.id_plan = prestadoras_planes.id_plan
            LEFT JOIN (
                SELECT id_afiliado, t2.id_estado, t2.fecha_alta, desc_estado
                FROM (
                    SELECT am.*
                    FROM afiliados_movimientos am
                        INNER JOIN(
                            SELECT id_afiliado, MAX(id_afiliado_movimiento) id_afiliado_movimiento
                            FROM afiliados_movimientos
                            GROUP BY id_afiliado
                        ) t1
                        ON am.id_afiliado = t1.id_afiliado
                        AND am.id_afiliado_movimiento = t1.id_afiliado_movimiento
                    ) t2
                    LEFT JOIN estados e ON e.id_estado = t2.id_estado
            ) afiliados_estados ON afiliados.id_afiliado = afiliados_estados.id_afiliado
            LEFT JOIN parentescos ON parentescos.id_parentesco = afiliados.id_parentesco

            WHERE afiliados.id_afiliado = $this->id_afiliado" );

        $result = $query->result()[0];

        return $result;
    }

    public function actualizarDatosPersonales($idCargo){
        return $this->db->query("UPDATE afiliados 
                                    SET 
                                        id_cargo = $idCargo
                                    WHERE id_afiliado = $this->id_afiliado "); 
    }

    public function obtenerDomicilios() {
        $query = $this->db->query(
            "SELECT
                --id
                domicilios.id_domicilio
                --datos del domilicilio legal/particular y postal, este recordset devuelve 2 registros
                ,domicilios.tipo_domicilio
                ,domicilios.calle
                ,domicilios.numero
                ,domicilios.piso
                ,domicilios.depto
                ,domicilios.referencia_adicional
                ,localidades.codigo_postal
                ,provincias.desc_provincia
                ,provincias.id_provincia
                ,localidades.desc_localidad
                ,localidades.id_localidad
                FROM domicilios
                    LEFT JOIN localidades ON localidades.id_localidad = domicilios.id_localidad
                    LEFT JOIN provincias ON provincias.id_provincia = localidades.id_provincia
                    --LEFT JOIN provincias provincias_as400 ON provincias_as400.id_provincia = domicilios.id_provincia_as400
                    LEFT JOIN afiliados ON afiliados.id_dato_adicional = domicilios.id_dato_adicional
            WHERE afiliados.id_afiliado = $this->id_afiliado");
        
        $result = $query->result(); //[0];

        return $result;
    }

    public function actualizarDomicilio($idDomicilio, $calle, $numero, $piso, $depto, $referenciaAdicional, $idLocalidad){
        
        $query = $this->db->query(
            "SELECT 
                id_dato_adicional
            FROM afiliados
            WHERE afiliados.id_afiliado = $this->id_afiliado");
        
        $idDatoAdicional = $query->row()->id_dato_adicional;

        return $this->db->query("UPDATE domicilios 
                                    SET 
                                        calle = '$calle'
                                        ,numero = '$numero'
                                        ,piso = '$piso'
                                        ,depto = '$depto'
                                        ,referencia_adicional = '$referenciaAdicional'
                                        ,id_localidad = $idLocalidad
                                        --,id_provincia_as400 = $
                                        --,codigo_postal_as400 = '$'
                                       
                                    WHERE id_domicilio = $idDomicilio AND id_dato_adicional = $idDatoAdicional "); 

                                    /* ,--id_provincia_as400 = $
                                        ,--codigo_postal_as400 = '$' */
    }

    public function obtenerDatosContacto() {
        $query = $this->db->query(
            "SELECT 
                datos_adicionales.telefono_fijo
                ,datos_adicionales.telefono_movil
                ,datos_adicionales.email1
                ,datos_adicionales.email2
                ,datos_adicionales.fax
            FROM datos_adicionales
                LEFT JOIN afiliados ON afiliados.id_dato_adicional = datos_adicionales.id_dato_adicional
            WHERE afiliados.id_afiliado = $this->id_afiliado");
        
        $result = $query->result()[0];

        return $result;
    }   

    public function actualizarDatosContacto($telefonoFijo, $telefonoMovil, $email1, $email2, $fax){
        $query = $this->db->query(
            "SELECT 
                id_dato_adicional
            FROM afiliados
            WHERE afiliados.id_afiliado = $this->id_afiliado");
        
        $idDatoAdicional = $query->row()->id_dato_adicional;
        
        return $this->db->query("UPDATE datos_adicionales 
                                    SET 
                                        datos_adicionales.telefono_fijo = '$telefonoFijo'
                                        ,datos_adicionales.telefono_movil = '$telefonoMovil'
                                        ,datos_adicionales.email1 = '$email1'
                                        ,datos_adicionales.email2 = '$email2'
                                        ,datos_adicionales.fax = '$fax'
                                    WHERE id_dato_adicional = $idDatoAdicional "); 
    }

    public function obtenerContable() {
        $query = $this->db->query(
            "SELECT
                --id
                    afiliados_contables.id_afiliado_contable
                -- datos del formulario
                ,condiciones_fiscales.desc_condicion_fiscal
                ,(SELECT TOP 1 tipos_comprobantes.letra 
                    FROM tipos_comprobantes_condiciones_fiscales
                        LEFT JOIN tipos_comprobantes ON tipos_comprobantes_condiciones_fiscales.id_tipo_comprobante = tipos_comprobantes.id_tipo_comprobante
                    WHERE tipos_comprobantes_condiciones_fiscales.id_origen = 1 AND	tipos_comprobantes_condiciones_fiscales.id_destino = afiliados_contables.id_condicion_fiscal
                ) comprobante
                ,desc_condicion_ganancia
                ,desc_condicion_iibb
                ,desc_condicion_iva
                ,afiliados_contables.facturar_a
                ,afiliados_contables.valor_corporativo
                ,empresas.razon_social empresa_cargo
                ,afiliados_contables.vigencia_cobertura
                ,afiliados_contables.empresa_facturar
                ,afiliados_contables.cuit_empresa_facturar
                ,afiliados_contables.acredita_reintegro
                ,afiliados_contables.cbu_acredita_reintegro
                ,afiliados_contables.id_banco_acredita_reintegro
                ,banco_acredita.desc_banco banco_acredita_reintegro
                ,afiliados_contables.tipo_cuenta_acredita_reintegro
                ,afiliados_contables.nro_cuenta_acredita_reintegro
                ,afiliados_contables.cuit_cuil_acredita_reintegro
                ,afiliados_contables.adherido_debito_autom
                ,afiliados_contables.cbu_debito_autom
                ,afiliados_contables.id_banco_debito_autom
                ,banco_debito_automatico.desc_banco banco_debito_automatico
                ,afiliados_contables.nro_cuenta_debito_autom
                ,afiliados_contables.cuit_cuil_debito_autom
                ,afiliados_contables.cheque_orden_de
                ,afiliados_contables.domicilio_cobro
                    
            FROM afiliados_contables 
                LEFT JOIN condiciones_fiscales on afiliados_contables.id_condicion_fiscal = condiciones_fiscales.id_condicion_fiscal
                LEFT JOIN condiciones_ganancias on afiliados_contables.id_condicion_ganancia = condiciones_ganancias.id_condicion_ganancia
                LEFT JOIN condiciones_iibb on afiliados_contables.id_condicion_iibb = condiciones_iibb.id_condicion_iibb
                LEFT JOIN condiciones_iva on afiliados_contables.id_condicion_iva = condiciones_iva.id_condicion_iva
                LEFT JOIN bancos banco_acredita on afiliados_contables.id_banco_acredita_reintegro = banco_acredita.id_banco
                LEFT JOIN bancos banco_debito_automatico on afiliados_contables.id_banco_debito_autom = banco_debito_automatico.id_banco
                LEFT JOIN empresas on afiliados_contables.id_empresa_cargo = empresas.id_empresa
                LEFT JOIN afiliados on afiliados_contables.id_afiliado = afiliados.id_afiliado
            WHERE
                afiliados.id_afiliado = $this->id_afiliado");
        
        $result = $query->result()[0];

        return $result;
    }

    public function actualizarContable($acredita_reintegro,$cbu_acredita_reintegro,$id_banco_acredita_reintegro,$tipo_cuenta_acredita_reintegro,$nro_cuenta_acredita_reintegro,$cuit_cuil_acredita_reintegro,$adherido_debito_autom,$cbu_debito_autom,$id_banco_debito_autom,$nro_cuenta_debito_autom,$cuit_cuil_debito_autom){
        
        $acreditaReintegro = (int)$acredita_reintegro;
        $adheridoDebitoAutom = (int)$adherido_debito_autom;

        $tipo = $tipo_cuenta_acredita_reintegro ? "'".$tipo_cuenta_acredita_reintegro."'" : "NULL";


        return $this->db->query("UPDATE afiliados_contables 
                                    SET 
                                        acredita_reintegro = $acreditaReintegro,
                                        cbu_acredita_reintegro = '$cbu_acredita_reintegro',
                                        id_banco_acredita_reintegro = $id_banco_acredita_reintegro,
                                        tipo_cuenta_acredita_reintegro = $tipo,
                                        nro_cuenta_acredita_reintegro = '$nro_cuenta_acredita_reintegro',
                                        cuit_cuil_acredita_reintegro = '$cuit_cuil_acredita_reintegro',
                                        adherido_debito_autom = $adheridoDebitoAutom,
                                        cbu_debito_autom = '$cbu_debito_autom',
                                        id_banco_debito_autom = $id_banco_debito_autom,
                                        nro_cuenta_debito_autom = '$nro_cuenta_debito_autom',
                                        cuit_cuil_debito_autom = '$cuit_cuil_debito_autom'

                                    WHERE id_afiliado = $this->id_afiliado "); 
    }

    public function obtenerBeneficiarios() {
        $query = $this->db->query(
            "SELECT * 
            FROM beneficiarios_fallecimientos
                LEFT JOIN tipos_documentos ON beneficiarios_fallecimientos.id_tipo_documento = tipos_documentos.id_tipo_documento
            WHERE
                beneficiarios_fallecimientos.id_afiliado = $this->id_afiliado");
        
        $result = $query->result();

        return $result;
    }

    public function actualizarBeneficiario($idBeneficiario, $nombreCompleto,
        $idTipoDocumento,$nroDocumento,$telefono,$email){
        return $this->db->query("UPDATE beneficiarios_fallecimientos 
                                    SET 
                                        nombre_completo = '$nombreCompleto',
                                        id_tipo_documento = $idTipoDocumento,
                                        nro_documento = '$nroDocumento',
                                        telefono = '$telefono',
                                        email = '$email'
                                    WHERE id_beneficiario_fallecimiento = $idBeneficiario AND id_afiliado = $this->id_afiliado "); 
    }

    public function insertarBeneficiario($nombreCompleto,$idTipoDocumento,
        $nroDocumento,$telefono,$email){
        
        return $this->db->query("INSERT into beneficiarios_fallecimientos 
                                    (id_afiliado,nombre_completo,id_tipo_documento,
                                    nro_documento,telefono,email)
                                VALUES
                                    ($this->id_afiliado,'$nombreCompleto',$idTipoDocumento,
                                    '$nroDocumento','$telefono','$email')"); 
    }

    public function eliminarBeneficiario($idBeneficiario){
        
        return $this->db->query("DELETE 
                                    FROM beneficiarios_fallecimientos 
                                    WHERE id_beneficiario_fallecimiento = $idBeneficiario AND id_afiliado = $this->id_afiliado "); 
    }

    public function obtenerGrupo() {
        $query = $this->db->query(
            "SELECT
                --id
                afiliados.id_afiliado
                --datos de grilla
                ,CONCAT (grupos_familiares.nro_grupo_afiliado, '-', right ('00' + convert(varchar,afiliados.digito_parentesco),2)) nro_afiliado
                ,afiliados.nombre_completo
                ,tipos_documentos.desc_tipo_documento
                ,afiliados.nro_documento
                ,afiliados.cuil_cuit
                ,parentescos.desc_parentesco
                ,CASE
                    WHEN dateadd(year, datediff (year, afiliados.fecha_nacimiento, getdate()), afiliados.fecha_nacimiento) > getdate()
                    THEN datediff (year, afiliados.fecha_nacimiento, getdate()) - 1
                    ELSE datediff (year, afiliados.fecha_nacimiento, getdate())
                END edad
                -- estado del afiliado
                ,afiliados_estados.id_estado
                ,afiliados_estados.desc_estado

            FROM afiliados
            LEFT JOIN sexos on sexos.id_sexo = afiliados.id_sexo
            LEFT JOIN grupos_familiares_afiliados ON grupos_familiares_afiliados.id_afiliado = afiliados.id_afiliado AND grupos_familiares_afiliados.fecha_hasta is NULL
            LEFT JOIN grupos_familiares ON grupos_familiares.id_grupo_familiar = grupos_familiares_afiliados.id_grupo_familiar
            LEFT JOIN tipos_documentos ON afiliados.id_tipo_documento = tipos_documentos.id_tipo_documento
            LEFT JOIN (
                SELECT id_afiliado, t2.id_estado, t2.fecha_alta, desc_estado
                FROM (
                    SELECT am.*
                    FROM afiliados_movimientos am
                        INNER JOIN(
                            SELECT id_afiliado, MAX(id_afiliado_movimiento) id_afiliado_movimiento
                            FROM afiliados_movimientos
                            GROUP BY id_afiliado
                        ) t1
                        ON am.id_afiliado = t1.id_afiliado
                        AND am.id_afiliado_movimiento = t1.id_afiliado_movimiento
                    ) t2
                    LEFT JOIN estados e ON e.id_estado = t2.id_estado
            ) afiliados_estados ON afiliados.id_afiliado = afiliados_estados.id_afiliado
            LEFT JOIN parentescos ON parentescos.id_parentesco = afiliados.id_parentesco

            WHERE afiliados.id_afiliado_padre = $this->id_afiliado");
        
        $result = $query->result(); //[0];

        return $result;
    } 
    
}