<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Afiliado extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
		
		//prevenimos caching de pagina previa
		$this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
		$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
		$this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
        $this->output->set_header('Pragma: no-cache');

        $this->load->library(array('form_validation'));

        // TODO: eliminar una vez que el login sea implementado
        $this->session->set_userdata('is_logued_in', true);
        $this->session->set_userdata('id_afiliado', 98469);
        $this->session->set_userdata('id_empresa', 2398);
        $this->session->set_userdata('id_prestador', 14155);
        $this->session->set_userdata('nombre', 'NOMBRE AFILIADO');

        // load models
        $this->load->model('Afiliados_model');
        $this->load->model('Diccionarios_model');

    }
    
    public function index()
	{
        $this->datosPersonales();
    }

    public function datosPersonales()
	{
        if(!$this->session->userdata('is_logued_in')){
			redirect(base_url());	
		}

        $data['title'] = "Afiliado | Perfil";
        $data['selectedMenu'] = "afiliado";
        $data['selectedSubmenu'] = "datos-personales";
		
        $data['datospersonales'] = (object) $this->Afiliados_model->obtenerDatosPersonales();
        $data['cargos'] = (object) $this->Diccionarios_model->obtenerCargos();
        
		$this->load->view('templates/header', $data);
		$this->load->view('pages/afiliado/datospersonales', $data);
		$this->load->view('templates/footer', $data);
    }

    public function actualizarDatosPersonales(){
        $this->output->set_content_type('application/json');
        $result = false;
        $errors = array();

        $this->form_validation->set_rules('id_cargo', 'cargo', 'required');
    
        if($this->form_validation->run()==false){ 
             $errors[] = validation_errors();
        }

        if (empty($errors)) {

            $idCargo = $this->input->post('id_cargo');
            $result = (object) $this->Afiliados_model->actualizarDatosPersonales($idCargo);

        }
        
		$response = array(
            'ok' => $result,
            'errors' => $errors
		);

		echo json_encode($response);
    }

    public function domicilio()
	{
        if(!$this->session->userdata('is_logued_in')){
			redirect(base_url());	
		}

        $data['title'] = "Afiliado | Domicilio";
        $data['selectedMenu'] = "afiliado";
        $data['selectedSubmenu'] = "domicilio";
		
        $domicilios = $this->Afiliados_model->obtenerDomicilios();
        
        foreach ($domicilios as $key => $value) {
            if($value->tipo_domicilio == 'Postal'){
                $data['domicilioPostal'] = $value;                
                $data['localidades_domicilioPostal'] = (object) $this->Diccionarios_model->obtenerLocalidades($value->id_provincia ?: 0);
                

            } else if($value->tipo_domicilio == 'Legal/Particular'){
                $data['domicilioLegal'] = $value;
                $data['localidades_domicilioLegal'] = (object) $this->Diccionarios_model->obtenerLocalidades($value->id_provincia ?: 0);
            }
        }

        $data['datosContacto'] = (object) $this->Afiliados_model->obtenerDatosContacto();
        $data['provincias'] = (object) $this->Diccionarios_model->obtenerProvincias();
        
		$this->load->view('templates/header', $data);
		$this->load->view('pages/afiliado/domicilio', $data);
		$this->load->view('templates/footer', $data);
    }

    public function actualizarDomicilio(){
        $this->output->set_content_type('application/json');
        $result = false;
        $errors = array();

        //$this->form_validation->set_rules('acredita_reintegro', 'acredita reintegro', 'required');
        //$this->form_validation->set_rules('adherido_debito_autom', 'adherido débito automático', 'required');
    
        // if($this->form_validation->run()==false){ 
        //      $errors[] = validation_errors();
        // }

        //if (empty($errors)) {

            $domicilioLegal_id_domicilio = $this->input->post('domicilioLegal_id_domicilio');
            $domicilioLegal_calle = $this->input->post('domicilioLegal_calle');
            $domicilioLegal_numero = $this->input->post('domicilioLegal_numero');
            $domicilioLegal_piso = $this->input->post('domicilioLegal_piso');
            $domicilioLegal_depto = $this->input->post('domicilioLegal_depto');
            $domicilioLegal_referencia_adicional = $this->input->post('domicilioLegal_referencia_adicional');
            //$domicilioLegal_codigo_postal = $this->input->post('domicilioLegal_codigo_postal');
            //$domicilioLegal_id_provincia = $this->input->post('domicilioLegal_id_provincia');
            $domicilioLegal_id_localidad = $this->input->post('domicilioLegal_id_localidad') ?: 0;

            $domicilioPostal_id_domicilio = $this->input->post('domicilioPostal_id_domicilio');
            $domicilioPostal_calle = $this->input->post('domicilioPostal_calle');
            $domicilioPostal_numero = $this->input->post('domicilioPostal_numero');
            $domicilioPostal_piso = $this->input->post('domicilioPostal_piso');
            $domicilioPostal_depto = $this->input->post('domicilioPostal_depto');
            $domicilioPostal_referencia_adicional = $this->input->post('domicilioPostal_referencia_adicional');
            //$domicilioPostal_codigo_postal = $this->input->post('domicilioPostal_codigo_postal');
            //$domicilioPostal_id_provincia = $this->input->post('domicilioPostal_id_provincia');
            $domicilioPostal_id_localidad = $this->input->post('domicilioPostal_id_localidad') ?: 0;

            $telefono_fijo = $this->input->post('telefono_fijo');
            $telefono_movil = $this->input->post('telefono_movil');
            $email1 = $this->input->post('email1');
            $email2 = $this->input->post('email2');
            $fax = $this->input->post('fax');

            $resultDomicilioLegal = (object) $this->Afiliados_model->actualizarDomicilio($domicilioLegal_id_domicilio, $domicilioLegal_calle, $domicilioLegal_numero, $domicilioLegal_piso, $domicilioLegal_depto, $domicilioLegal_referencia_adicional, $domicilioLegal_id_localidad);
            $resultDomicilioPostal = (object) $this->Afiliados_model->actualizarDomicilio($domicilioPostal_id_domicilio, $domicilioPostal_calle, $domicilioPostal_numero, $domicilioPostal_piso, $domicilioPostal_depto, $domicilioPostal_referencia_adicional, $domicilioPostal_id_localidad);
            $resultDatosPersonales = (object) $this->Afiliados_model->actualizarDatosContacto($telefono_fijo, $telefono_movil, $email1, $email2, $fax);

            $result = $resultDomicilioLegal && $resultDomicilioPostal && $resultDatosPersonales;            

        //}
        
		$response = array(
            'ok' => $result,
            'errors' => $errors
		);

		echo json_encode($response);
    }

    public function contable()
	{
        if(!$this->session->userdata('is_logued_in')){
			redirect(base_url());	
		}

        $data['title'] = "Afiliado | Contable";
        $data['selectedMenu'] = "afiliado";
        $data['selectedSubmenu'] = "contable";
		
        $data['contable'] = (object) $this->Afiliados_model->obtenerContable();
        $data['bancos'] = (object) $this->Diccionarios_model->obtenerBancos();
        
		$this->load->view('templates/header', $data);
		$this->load->view('pages/afiliado/contable', $data);
		$this->load->view('templates/footer', $data);
    }

    public function actualizarContable(){
        $this->output->set_content_type('application/json');
        $result = false;
        $errors = array();

        //$this->form_validation->set_rules('acredita_reintegro', 'acredita reintegro', 'required');
        //$this->form_validation->set_rules('adherido_debito_autom', 'adherido débito automático', 'required');
    
        // if($this->form_validation->run()==false){ 
        //      $errors[] = validation_errors();
        // }

        if (empty($errors)) {

            $acredita_reintegro = $this->input->post('acredita_reintegro') == "on";           
            $cbu_acredita_reintegro = $this->input->post('cbu_acredita_reintegro');
            $id_banco_acredita_reintegro = (int)$this->input->post('id_banco_acredita_reintegro') ?: 0;
            $tipo_cuenta_acredita_reintegro = $this->input->post('tipo_cuenta_acredita_reintegro');
            $nro_cuenta_acredita_reintegro = $this->input->post('nro_cuenta_acredita_reintegro');
            $cuit_cuil_acredita_reintegro = $this->input->post('cuit_cuil_acredita_reintegro');
            $adherido_debito_autom = $this->input->post('adherido_debito_autom') == "on";
            $cbu_debito_autom = $this->input->post('cbu_debito_autom');
            $id_banco_debito_autom = (int)$this->input->post('id_banco_debito_autom') ?: 0;
            $nro_cuenta_debito_autom = $this->input->post('nro_cuenta_debito_autom');
            $cuit_cuil_debito_autom = $this->input->post('cuit_cuil_debito_autom');

            

            $result = (object) $this->Afiliados_model->actualizarContable($acredita_reintegro,$cbu_acredita_reintegro,$id_banco_acredita_reintegro,$tipo_cuenta_acredita_reintegro,$nro_cuenta_acredita_reintegro,$cuit_cuil_acredita_reintegro,$adherido_debito_autom,$cbu_debito_autom,$id_banco_debito_autom,$nro_cuenta_debito_autom,$cuit_cuil_debito_autom);

        }
        
		$response = array(
            'ok' => $result,
            'errors' => $errors
		);

		echo json_encode($response);
    }

    public function beneficiarios()
	{
        if(!$this->session->userdata('is_logued_in')){
			redirect(base_url());	
		}

        $data['title'] = "Afiliado | Beneficiarios";
        $data['selectedMenu'] = "afiliado";
        $data['selectedSubmenu'] = "beneficiarios";
        $data['modales']  = ['eliminar_beneficiario'];
		
        $data['beneficiarios'] = (object) $this->Afiliados_model->obtenerBeneficiarios();
        $data['tiposDocumentos'] = (object) $this->Diccionarios_model->obtenerTiposDocumentosValidos();
        
		$this->load->view('templates/header', $data);
		$this->load->view('pages/afiliado/beneficiarios', $data);
		$this->load->view('templates/footer', $data);
    }

    public function guardarBeneficiario(){
        $this->output->set_content_type('application/json');
        $result = false;
        $errors = array();

        $this->form_validation->set_rules('nombre_completo', 'nombre completo', 'required');
        $this->form_validation->set_rules('id_tipo_documento', 'tipo de documento', 'required');
        $this->form_validation->set_rules('nro_documento', 'número de documento', 'required|integer');
    
        if($this->form_validation->run()==false){ 
             $errors[] = validation_errors();
        }

        if (empty($errors)) {

            $idBeneficiario = $this->input->post('id_beneficiario_fallecimiento');
            $nombreCompleto = $this->input->post('nombre_completo');
            $idTipoDocumento = $this->input->post('id_tipo_documento');
            $nroDocumento = $this->input->post('nro_documento');
            $telefono = $this->input->post('telefono');
            $email = $this->input->post('email');

            if($idBeneficiario){
                $result = (object) $this->Afiliados_model->actualizarBeneficiario($idBeneficiario, 
                    $nombreCompleto,$idTipoDocumento,$nroDocumento,$telefono,$email);
            }
            else{
                $result = (object) $this->Afiliados_model->insertarBeneficiario($nombreCompleto,
                    $idTipoDocumento,$nroDocumento,$telefono,$email);
            }
        }
        
		$response = array(
            'ok' => $result,
            'errors' => $errors
		);

		echo json_encode($response);
    }

    public function eliminarbeneficiario(){
        $this->output->set_content_type('application/json');

		$id = $this->input->post('id');
		$result = true;
		
		$result = $this->Afiliados_model->eliminarBeneficiario($id);

		$response = array(
			'ok' => $result
		);

		echo json_encode($response);	
    }

    public function grupo()
	{
        if(!$this->session->userdata('is_logued_in')){
			redirect(base_url());	
		}

        $data['title'] = "Afiliado | Grupo";
        $data['selectedMenu'] = "afiliado";
        $data['selectedSubmenu'] = "grupo";
		
        $data['grupo'] =  (object) $this->Afiliados_model->obtenerGrupo();
        
		$this->load->view('templates/header', $data);
		$this->load->view('pages/afiliado/grupo', $data);
		$this->load->view('templates/footer', $data);
    }

    // public function obtenerLocalidades(){
    //     $this->output->set_content_type('application/json');

    //     $id = $this->input->get('id_provincia');
    //     $search = $this->input->get('search');
	// 	$result = true;
                
    //     //$result = (object) $this->Afiliados_model->buscarLocalidades($id, $search, 30);
    //     $result = (object) $this->Diccionarios_model->obtenerLocalidades($id);

	// 	$response = array(
	// 		'data' => $result
	// 	);

	// 	echo json_encode($response);	
    // }

    // public function obtenerCodigosPostales(){
    //     $this->output->set_content_type('application/json');

    //     $id = $this->input->get('id_provincia');
    //     $search = $this->input->get('search');
	// 	$result = true;
                
	// 	$result = (object) $this->Diccionarios_model->buscarLocalidadesPorCodPostal($id, $search, 30);

	// 	$response = array(
	// 		'data' => $result
	// 	);

	// 	echo json_encode($response);	
    // }
    

	
	
}
