<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Empresa extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
		
		//prevenimos caching de pagina previa
		$this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
		$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
		$this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
        $this->output->set_header('Pragma: no-cache');

        $this->load->library(array('form_validation'));


        // TODO: eliminar una vez que el login sea implementado
        $this->session->set_userdata('is_logued_in', true);
        $this->session->set_userdata('id_afiliado', 98469);
        $this->session->set_userdata('id_empresa', 2398);
        $this->session->set_userdata('id_prestador', 14155);
        $this->session->set_userdata('nombre', 'NOMBRE AFILIADO');
        
        // load models
        $this->load->model('Empresas_model');
        $this->load->model('Diccionarios_model');

    }
    
    public function index()
	{
        $this->datos_generales();
    }

    public function datos_generales()
	{
        if(!$this->session->userdata('is_logued_in')){
			redirect(base_url());	
		}

        $data['title'] = "Empresa | Perfil";
        $data['selectedMenu'] = "empresa";
        $data['selectedSubmenu'] = "datos-generales";
		
        $data['datosGenerales'] = (object) $this->Empresas_model->obtenerDatosGenerales();
        $data['tiposSociedades'] = (object) $this->Diccionarios_model->obtenerTiposSociedades();
        $data['rubros'] = (object) $this->Diccionarios_model->obtenerRubros();
        
		$this->load->view('templates/header', $data);
		$this->load->view('pages/empresa/datos_generales', $data);
		$this->load->view('templates/footer', $data);
    }

    public function actualizarDatosGenerales(){
        $this->output->set_content_type('application/json');
        $result = false;
        $errors = array();

        $this->form_validation->set_rules('razon_social', 'razón social', 'required');
    
        if($this->form_validation->run()==false){ 
             $errors[] = validation_errors();
        }

        if (empty($errors)) {

            $razon_social = $this->input->post('razon_social');
            $fecha_inicio_actividad = $this->input->post('fecha_inicio_actividad');
            $id_tipo_sociedad = $this->input->post('id_tipo_sociedad');
            $id_rubro = $this->input->post('id_rubro');
            $cuenta = $this->input->post('cuenta');
            $result = (object) $this->Empresas_model->actualizarDatosGenerales($razon_social,$fecha_inicio_actividad,$id_tipo_sociedad,$id_rubro,$cuenta);

        }
        
		$response = array(
            'ok' => $result,
            'errors' => $errors
		);

		echo json_encode($response);
    }

    public function domicilio()
	{
        if(!$this->session->userdata('is_logued_in')){
			redirect(base_url());	
		}

        $data['title'] = "Empresa | Domicilio";
        $data['selectedMenu'] = "empresa";
        $data['selectedSubmenu'] = "domicilio";
		
        $domicilios = $this->Empresas_model->obtenerDomicilios();
        
        foreach ($domicilios as $key => $value) {
            if($value->tipo_domicilio == 'Postal'){
                $data['domicilioPostal'] = $value;                
                $data['localidades_domicilioPostal'] = (object) $this->Diccionarios_model->obtenerLocalidades($value->id_provincia ?: 0);
                

            } else if($value->tipo_domicilio == 'Legal/Particular'){
                $data['domicilioLegal'] = $value;
                $data['localidades_domicilioLegal'] = (object) $this->Diccionarios_model->obtenerLocalidades($value->id_provincia ?: 0);
            }
        }

        $domicilios = $this->Empresas_model->obtenerDomicilios();
        $data['datosContacto'] = (object) $this->Empresas_model->obtenerDatosContacto();
        $data['provincias'] = (object) $this->Diccionarios_model->obtenerProvincias();
        
		$this->load->view('templates/header', $data);
		$this->load->view('pages/empresa/domicilio', $data);
		$this->load->view('templates/footer', $data);
    }

    public function actualizarDomicilio(){
        $this->output->set_content_type('application/json');
        $result = false;
        $errors = array();

        //$this->form_validation->set_rules('acredita_reintegro', 'acredita reintegro', 'required');
        //$this->form_validation->set_rules('adherido_debito_autom', 'adherido débito automático', 'required');
    
        // if($this->form_validation->run()==false){ 
        //      $errors[] = validation_errors();
        // }

        // if (empty($errors)) {

            $domicilioLegal_id_domicilio = $this->input->post('domicilioLegal_id_domicilio');
            $domicilioLegal_calle = $this->input->post('domicilioLegal_calle');
            $domicilioLegal_numero = $this->input->post('domicilioLegal_numero');
            $domicilioLegal_piso = $this->input->post('domicilioLegal_piso');
            $domicilioLegal_depto = $this->input->post('domicilioLegal_depto');
            $domicilioLegal_referencia_adicional = $this->input->post('domicilioLegal_referencia_adicional');
            $domicilioLegal_id_localidad = $this->input->post('domicilioLegal_id_localidad') ?: 0;

            $domicilioPostal_id_domicilio = $this->input->post('domicilioPostal_id_domicilio');
            $domicilioPostal_calle = $this->input->post('domicilioPostal_calle');
            $domicilioPostal_numero = $this->input->post('domicilioPostal_numero');
            $domicilioPostal_piso = $this->input->post('domicilioPostal_piso');
            $domicilioPostal_depto = $this->input->post('domicilioPostal_depto');
            $domicilioPostal_referencia_adicional = $this->input->post('domicilioPostal_referencia_adicional');
            $domicilioPostal_id_localidad = $this->input->post('domicilioPostal_id_localidad') ?: 0;

            $telefono_fijo = $this->input->post('telefono_fijo');
            $telefono_movil = $this->input->post('telefono_movil');
            $email1 = $this->input->post('email1');
            $email2 = $this->input->post('email2');
            $fax = $this->input->post('fax');

            $resultDomicilioLegal = (object) $this->Empresas_model->actualizarDomicilio($domicilioLegal_id_domicilio, $domicilioLegal_calle, $domicilioLegal_numero, $domicilioLegal_piso, $domicilioLegal_depto, $domicilioLegal_referencia_adicional, $domicilioLegal_id_localidad);
            $resultDomicilioPostal = (object) $this->Empresas_model->actualizarDomicilio($domicilioPostal_id_domicilio, $domicilioPostal_calle, $domicilioPostal_numero, $domicilioPostal_piso, $domicilioPostal_depto, $domicilioPostal_referencia_adicional, $domicilioPostal_id_localidad);
            $resultDatosPersonales = (object) $this->Empresas_model->actualizarDatosContacto($telefono_fijo, $telefono_movil, $email1, $email2, $fax);

            $result = $resultDomicilioLegal && $resultDomicilioPostal && $resultDatosPersonales;            

        //}
        
		$response = array(
            'ok' => $result,
            'errors' => $errors
		);

		echo json_encode($response);
    }

    	
}
