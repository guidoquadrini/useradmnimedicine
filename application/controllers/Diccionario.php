<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Diccionario extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
		
		//prevenimos caching de pagina previa
		$this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
		$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
		$this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
        $this->output->set_header('Pragma: no-cache');

        // load models
        $this->load->model('Diccionarios_model');

    }
    
    public function obtenerLocalidades(){
        $this->output->set_content_type('application/json');

        $id = $this->input->get('id_provincia');
        $search = $this->input->get('search');
		$result = true;
                
        //$result = (object) $this->Afiliados_model->buscarLocalidades($id, $search, 30);
        $result = (object) $this->Diccionarios_model->obtenerLocalidades($id);

		$response = array(
			'data' => $result
		);

		echo json_encode($response);	
    }

    public function obtenerCodigosPostales(){
        $this->output->set_content_type('application/json');

        $id = $this->input->get('id_provincia');
        $search = $this->input->get('search');
		$result = true;
                
		$result = (object) $this->Diccionarios_model->buscarLocalidadesPorCodPostal($id, $search, 30);

		$response = array(
			'data' => $result
		);

		echo json_encode($response);	
    }
	
}
