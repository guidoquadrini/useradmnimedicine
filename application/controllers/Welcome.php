<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
		
		//prevenimos caching de pagina previa
		$this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
		$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
		$this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
        $this->output->set_header('Pragma: no-cache');
        
        // TODO: eliminar una vez que el login sea implementado
        $this->session->set_userdata('is_logued_in', true);
        $this->session->set_userdata('id_afiliado', 98469);
        $this->session->set_userdata('id_empresa', 2398);
        $this->session->set_userdata('id_prestador', 14155);
        $this->session->set_userdata('nombre', 'NOMBRE AFILIADO');

    }

	public function index()
	{
		$data['title'] = "OPDEA"; // Capitalize the first letter
		$data['selectedMenu'] = "";
		$data['selectedSubmenu'] = "";
    
		$this->load->view('templates/header', $data);
		$this->load->view('welcome_message', $data);
		$this->load->view('templates/footer', $data);
	}
}
