<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prestador extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
		
		//prevenimos caching de pagina previa
		$this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
		$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
		$this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
        $this->output->set_header('Pragma: no-cache');

        $this->load->library(array('form_validation'));

        // TODO: eliminar una vez que el login sea implementado
        $this->session->set_userdata('is_logued_in', true);
        $this->session->set_userdata('id_afiliado', 98469);
        $this->session->set_userdata('id_empresa', 2398);
        $this->session->set_userdata('id_prestador', 14155);
        $this->session->set_userdata('nombre', 'NOMBRE AFILIADO');

        // load models
        $this->load->model('Prestadores_model');
        $this->load->model('Diccionarios_model');

    }
    
    public function index()
	{
        $this->datos_generales();
    }

    public function datos_generales()
	{

        if(!$this->session->userdata('is_logued_in')){
			redirect(base_url());	
		}

        $data['title'] = "Prestador | Datos Generales";
        $data['selectedMenu'] = "prestador";
        $data['selectedSubmenu'] = "datos-generales";
		
        $data['datosGenerales'] = (object) $this->Prestadores_model->obtenerDatosGenerales();
        $data['titulos'] = (object) $this->Diccionarios_model->obtenerTitulosAbreviados();
        
		$this->load->view('templates/header', $data);
		$this->load->view('pages/prestador/datos_generales', $data);
		$this->load->view('templates/footer', $data);
    }

    public function actualizarDatosGenerales(){
        $this->output->set_content_type('application/json');
        $result = false;
        $errors = array();

        $this->form_validation->set_rules('nombre_razon_social', 'nombre completo/razón social', 'required');
    
        if($this->form_validation->run()==false){ 
             $errors[] = validation_errors();
        }

        if (empty($errors)) {

            $id_titulo_abreviado = $this->input->post('id_titulo_abreviado');
            $nombre_razon_social = $this->input->post('nombre_razon_social');
            $fecha_nacimiento = $this->input->post('fecha_nacimiento');
            $inos_anssal = $this->input->post('inos_anssal');
            $fecha_inicio_actividad = $this->input->post('fecha_inicio_actividad');
            $matricula_provincial = $this->input->post('matricula_provincial');
            $matricula_nacional = $this->input->post('matricula_nacional');
            $circulo = $this->input->post('circulo') == "on";  
            $result = (object) $this->Prestadores_model->actualizarDatosPersonales($id_titulo_abreviado,$nombre_razon_social,$fecha_nacimiento,
                $inos_anssal,$fecha_inicio_actividad,$matricula_provincial,$matricula_nacional,$circulo);

        }
        
		$response = array(
            'ok' => $result,
            'errors' => $errors
		);

		echo json_encode($response);
    }

    public function domicilio()
	{
        if(!$this->session->userdata('is_logued_in')){
			redirect(base_url());	
		}

        $data['title'] = "Prestador | Domicilio";
        $data['selectedMenu'] = "prestador";
        $data['selectedSubmenu'] = "domicilio";
		
        //$domicilios = $this->Empresas_model->obtenerDomicilios();

        $data['domicilio'] = (object) $this->Prestadores_model->obtenerDomicilios();
        $data['localidades'] = (object) $this->Diccionarios_model->obtenerLocalidades($data['domicilio']->id_provincia ?: 0);
        $data['provincias'] = (object) $this->Diccionarios_model->obtenerProvincias();
        
		$this->load->view('templates/header', $data);
		$this->load->view('pages/prestador/domicilio', $data);
		$this->load->view('templates/footer', $data);
    }

    public function actualizarDomicilio(){
        $this->output->set_content_type('application/json');
        $result = false;
        $errors = array();

        //$this->form_validation->set_rules('acredita_reintegro', 'acredita reintegro', 'required');
        //$this->form_validation->set_rules('adherido_debito_autom', 'adherido débito automático', 'required');
    
        // if($this->form_validation->run()==false){ 
        //      $errors[] = validation_errors();
        // }

        //if (empty($errors)) {

            $domicilioLegal_id_domicilio = $this->input->post('domicilioLegal_id_domicilio');
            $domicilioLegal_calle = $this->input->post('domicilioLegal_calle');
            $domicilioLegal_numero = $this->input->post('domicilioLegal_numero');
            $domicilioLegal_piso = $this->input->post('domicilioLegal_piso');
            $domicilioLegal_depto = $this->input->post('domicilioLegal_depto');
            $domicilioLegal_referencia_adicional = $this->input->post('domicilioLegal_referencia_adicional');
            $domicilioLegal_id_localidad = $this->input->post('domicilioLegal_id_localidad') ?: 0;

            $telefono_fijo = $this->input->post('telefono_fijo');
            $telefono_movil = $this->input->post('telefono_movil');
            $email1 = $this->input->post('email1');
            $email2 = $this->input->post('email2');
            $fax = $this->input->post('fax');

            $resultDomicilioLegal = (object) $this->Prestadores_model->actualizarDomicilio($domicilioLegal_calle, $domicilioLegal_numero, $domicilioLegal_piso, $domicilioLegal_depto, $domicilioLegal_referencia_adicional, $domicilioLegal_id_localidad);
            $resultDatosPersonales = (object) $this->Prestadores_model->actualizarDatosContacto($telefono_fijo, $telefono_movil, $email1, $email2, $fax);

            $result = $resultDomicilioLegal && $resultDatosPersonales;            

        //}
        
		$response = array(
            'ok' => $result,
            'errors' => $errors
		);

		echo json_encode($response);
    }

    	
}
