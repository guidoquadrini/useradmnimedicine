<!-- Mini Modal -->
<div class="modal fade modal-mini modal-primary" id="modalEliminarBeneficiario" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- <div class="modal-header justify-content-center">
                <div class="modal-profile">
                    <i class="now-ui-icons users_circle-08"></i>
                </div>
            </div> -->
            <div class="modal-body">
                <p>¿Confirmas que deseas borrar el beneficiario <b><span class="text">Juan Pérez</span></b>?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link btn-neutral" data-dismiss="modal">Cancelar</button>
                <button type="button" name="eliminar" class="btn btn-link btn-neutral">Eliminar</button>
            </div>
        </div>
    </div>
</div>
<!--  End Modal -->