<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title><?php echo $title; ?></title>

    <link rel="shortcut icon" href="<?php echo base_url();?>favicon.ico">

    <!-- Font -->
    <link rel="dns-prefetch" href="//fonts.googleapis.com">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css?v=4.4.1">
    <!-- BootToast CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootoast.min.css">
    <!-- Font-awesome Icons -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
    <!-- Now UI Kit -->
    <link href="<?php echo base_url();?>assets/css/now-ui-kit.min.css?v=1.2.0" rel="stylesheet">
    <!-- Now UI Kit -->
    <link href="<?php echo base_url();?>assets/css/now-ui-kit.custom.css?v=1.2.0" rel="stylesheet">
    <!-- Main css -->
    <link href="<?php echo base_url();?>assets/css/usuarios.css?t=20180924" rel="stylesheet">

</head>

<body>

    <div class="wrapper">
        <!-- Sidebar Holder -->
        <nav id="sidebar"  class="bg-light">
            <div class="sticky-top">
            <div class="pb-1 pt-4 d-flex flex-column text-center">
                <img src="<?php echo base_url();?>assets/img/default-avatar.png" alt="client" class="rounded-circle mx-auto mb-3 img_perfil" width="80">
                <h6><?php echo $this->session->userdata('nombre') ?></h6>
            </div>

            <hr>
            
            <ul class="nav flex-column mx-2" id="sidebarMenu">
                
                <li class="nav-item">
                    <a href="#afiliadoSubmenu" data-toggle="collapse" aria-expanded="<?php if($selectedMenu == 'afiliado') echo ("true"); else  echo ("false");?>" class="dropdown-toggle nav-link">Perfil Afiliado</a>
                    <ul class="collapse <?php if($selectedMenu == 'afiliado' ) echo ("show") ?> list-unstyled pl-3" id="afiliadoSubmenu" data-parent="#sidebarMenu">
                        <li>
                            <a class="nav-link <?php if($selectedSubmenu == 'datos-personales') echo ("active") ?>" href="<?php echo base_url();?>afiliado/datospersonales">Datos personales</a>
                        </li>
                        <li>
                            <a class="nav-link <?php if($selectedSubmenu == 'domicilio') echo ("active") ?>" href="<?php echo base_url();?>afiliado/domicilio">Domicilio</a>
                        </li>
                        <li>
                            <a class="nav-link <?php if($selectedSubmenu == 'contable') echo ("active") ?>" href="<?php echo base_url();?>afiliado/contable">Contable</a>
                        </li>
                        <li>
                            <a class="nav-link <?php if($selectedSubmenu == 'beneficiarios') echo ("active") ?>" href="<?php echo base_url();?>afiliado/beneficiarios">Beneficiarios</a>
                        </li>
                        <li>
                            <a class="nav-link <?php if($selectedSubmenu == 'grupo') echo ("active") ?>" href="<?php echo base_url();?>afiliado/grupo">Grupo</a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="#empresaSubmenu" data-toggle="collapse" aria-expanded="<?php if($selectedMenu == 'empresa') echo ("true"); else  echo ("false");?>" class="dropdown-toggle nav-link">Perfil Empresa</a>
                    <ul class="collapse <?php if($selectedMenu == 'empresa' ) echo ("show") ?> list-unstyled pl-3" id="empresaSubmenu" data-parent="#sidebarMenu">
                        <li>
                            <a class="nav-link <?php if($selectedSubmenu == 'datos-generales') echo ("active") ?>" href="<?php echo base_url();?>empresa/datos-generales">Datos generales</a>
                        </li>
                        <li>
                            <a class="nav-link <?php if($selectedSubmenu == 'domicilio') echo ("active") ?>" href="<?php echo base_url();?>empresa/domicilio">Domicilio</a>
                        </li>                        
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="#prestadorSubmenu" data-toggle="collapse" aria-expanded="<?php if($selectedMenu == 'prestador') echo ("true"); else  echo ("false");?>" class="dropdown-toggle nav-link">Perfil Prestador</a>
                    <ul class="collapse <?php if($selectedMenu == 'prestador' ) echo ("show") ?> list-unstyled pl-3" id="prestadorSubmenu" data-parent="#sidebarMenu">
                        <li>
                            <a class="nav-link <?php if($selectedSubmenu == 'datos-generales') echo ("active") ?>" href="<?php echo base_url();?>prestador/datos-generales">Datos generales</a>
                        </li>
                        <li>
                            <a class="nav-link <?php if($selectedSubmenu == 'domicilio') echo ("active") ?>" href="<?php echo base_url();?>prestador/domicilio">Domicilio</a>
                        </li>                        
                    </ul>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Gestión Solicitudes</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Solicitar Credencial</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Consultar autorización médica</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Solicitar insumo médico</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Solicitar presupuesto reintegro</a>
                </li>
            </ul>
            </div>
        </nav>

        <!-- Page Content Holder -->
        <div id="content">
            <nav class="navbar bg-primary sticky-top">
                <div class="container-fluid">
                    <div class="d-flex">

                        <button type="button" id="sidebarCollapse" class="navbar-btn">
                            <span class="navbar-toggler-bar bar1"></span>
                            <span class="navbar-toggler-bar bar2"></span>
                            <span class="navbar-toggler-bar bar3"></span>
                        </button>

                        <a class="navbar-brand ml-3 p-0 my-auto" href="<?php echo base_url();?>">
                            <img src="<?php echo base_url();?>assets/img/logo.png" alt="logo opdea" height="40px"/>
                        </a>
                    </div>

                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo base_url().'login/logout' ?>">
                                <i class="now-ui-icons media-1_button-power"></i>
                                <p>Salir</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
            <div class="content px-3 mb-5">