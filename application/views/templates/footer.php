            </div>

            <footer class="py-2 text-center w-100" data-background-color="black">
                <p class="mb-0">
                    <small>
                        Obra Social del Personal de Dirección © OPDEA
                        <script>
                            document.write(new Date().getFullYear())
                        </script>
                    </small>
                </p>
            </footer>

        </div>
    </div>

    <?php
    if(isset($modales)){
        foreach($modales as $modal) { 
            $this->load->view('modales/'.$modal);
        }
    }
    ?>

    <!--   Core JS Files   -->
    <script src="<?php echo base_url();?>assets/js/core/jquery.min.js?v=3.3.1" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/js/core/popper.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/js/core/bootstrap.min.js?v=4.1.1" type="text/javascript"></script>
    <!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
    <script src="<?php echo base_url();?>assets/js/plugins/bootstrap-switch.js"></script>
    <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
    <script src="<?php echo base_url();?>assets/js/plugins/nouislider.min.js" type="text/javascript"></script>
    <!--  Plugin for the BootToast, full documentation here: https://github.com/odahcam/bootoast -->
    <script src="<?php echo base_url();?>assets/js/plugins/bootoast.min.js" type="text/javascript"></script>
    <!-- Bootstrap Typeahead Plugin -->
    <script src="<?php echo base_url();?>assets/js/plugins/bootstrap3-typeahead.min.js"></script>
    <!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
        <script src="<?php echo base_url();?>assets/js/plugins/bootstrap-datepicker.js" type="text/javascript"></script>
    <!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
    <script src="<?php echo base_url();?>assets/js/now-ui-kit.min.js?v=1.2.0" type="text/javascript"></script>
    


    <!-- <script src="<?php echo base_url();?>assets/js/usuario.js?v=1.1.0" type="text/javascript"></script> -->
    <script src="<?php echo base_url();?>assets/js/main.js?v=1.1.0" type="text/javascript"></script>
    

    <script type="text/javascript">
        $(document).ready(function () {

            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
                $(this).toggleClass('active');
            });

            $('.reloadButton').on('click', function () {
                window.location.href = window.location.href;
            });

            /* Combo localidades */
            $("#domicilioLegal_id_provincia, #domicilioPostal_id_provincia").on("change", function(event){
                
                var idProv = $(event.target).val();
                var localidad = $(event.target).data("localidad");

                $localidad = $("[name='" + localidad + "']");
                $localidad.val("");

                $.getJSON("<?php echo base_url();?>diccionario/obtenerLocalidades?id_provincia=" + idProv, function(res) {

                    var data = [];
                    for(var i in res.data)
                    {
                        var element = res.data[i];
                        data.push(element);
                    }
                    $localidad.data('typeahead').source = data;
                });
                
            });
                         

             $(".localidades-typeahead").typeahead({ 

                // data source
                source: [],

                displayText: function(item){
                    //return item.desc_localidad;
                    return '(' + item.codigo_postal + ') ' + item.desc_localidad;
                },

                // how many items to show
                items: 10,

                // default template
                menu: '<ul class="typeahead dropdown-menu" role="listbox"></ul>',
                item: '<li><a class="dropdown-item" href="#" role="option"></a></li>',
                headerHtml: '<li class="dropdown-header"></li>',
                headerDivider: '<li class="divider" role="separator"></li>',
                itemContentSelector:'a',

                // min length to trigger the suggestion list
                minLength: 1,

                // number of pixels the scrollable parent container scrolled down
                scrollHeight: 0,

                // auto selects the first item
                autoSelect: false,

                showHintOnFocus: 'all',

                // callbacks
                afterSelect: function(event){
                    var $input = $($(this)[0].$element[0]);
                    $input.closest(".form-group").find("[type=hidden]").val(event.id_localidad);
                },
                afterEmptySelect: function(event){
                    var $input = $($(this)[0].$element[0]);
                    $input.val("");
                    $input.closest(".form-group").find("[type=hidden]").val("");
                },

                // adds an item to the end of the list
                addItem: false,

                // delay between lookups
                delay: 0,

                showHintOnFocus: true

            });

            //load initial source based on prov selected (postal and legal)
            $domicilioLegalProv = $("[name=domicilioLegal_id_provincia]");
            
            if($domicilioLegalProv.length > 0){
                $domicilioLegalLocalidad = $("#domicilioLegal_desc_localidad");
                //$domicilioLegalCodPostal = $("[name=domicilioLegal_codigo_postal]");

                $.getJSON("<?php echo base_url();?>diccionario/obtenerLocalidades?id_provincia=" + $domicilioLegalProv.val(), function(res) {
                    var data = [];
                    for(var i in res.data)
                    {
                        var element = res.data[i];
                        data.push(element);
                        //options += "<option value='" + element.id_localidad +"' data-codigopostal='" + element.codigo_postal +"'>" + element.desc_localidad +"</option>";
                    }
                    $domicilioLegalLocalidad.data('typeahead').source = data;
                    //$domicilioLegalCodPostal.data('typeahead').source = data;
                });
            }

            $domicilioPostalProv = $("[name=domicilioPostal_id_provincia]");
            if($domicilioPostalProv.length > 0){
                $domicilioPostalLocalidad = $("#domicilioPostal_desc_localidad");

                $.getJSON("<?php echo base_url();?>diccionario/obtenerLocalidades?id_provincia=" + $domicilioPostalProv.val(), function(res) {
                    var data = [];
                    for(var i in res.data)
                    {
                        var element = res.data[i];
                        data.push(element);
                    }
                    $domicilioPostalLocalidad.data('typeahead').source = data;
                });
            }
            /* /Combo localidades */

            /* --------- 1. PERFIL AFILIADOS ----------- */
            
            /* 1.1 Datos personales                    */
            
            $('form#formDatosPersonales').on('submit',function(event){
                event.preventDefault();
                $form = $(this);
                $buttons = $form.find("button");
                $inputs = $form.find("input:not([disabled])");
                
                var url = $form.attr("action");
                var data = $form.serialize();

                $buttons.attr("disabled",true);
                $inputs.attr("disabled",true);

                $.post(url, data)
                .done(function(res) {
                    console.log(res);
                    if(res.ok){
                        showSuccessToast("Los cambios han sido guardados.");
                    }
                    else{
                        if(res.errors.length > 0){
                            showErrorToast(res.errors[0]);
                        }
                        else{
                            showErrorToast("Lo sentimos, ha ocurrido un error.");
                        }
                    }
                })
                .fail(function(err) {
                    showErrorToast("Lo sentimos, ha ocurrido un error.");
                })
                .always(function(data) {
                    $buttons.attr("disabled",false);
                    $inputs.attr("disabled",false);
                });

            });

            /* /1.1 Datos personales */

            /* 1.2 Domicilios */
            
            $('form#formDomicilios').on('submit',function(event){
                event.preventDefault();
                $form = $(this);
                $buttons = $form.find("button");
                $inputs = $form.find("input:not([disabled]),select:not([disabled])");
                
                var url = $form.attr("action");
                var data = $form.serialize();

                $buttons.attr("disabled",true);
                $inputs.attr("disabled",true);

                $.post(url, data)
                .done(function(res) {
                    console.log(res);
                    if(res.ok){
                        showSuccessToast("Los cambios han sido guardados.");
                    }
                    else{
                        if(res.errors.length > 0){
                            showErrorToast(res.errors[0]);
                        }
                        else{
                            showErrorToast("Lo sentimos, ha ocurrido un error.");
                        }
                    }
                })
                .fail(function(err) {
                    showErrorToast("Lo sentimos, ha ocurrido un error.");
                })
                .always(function(data) {
                    $buttons.attr("disabled",false);
                    $inputs.attr("disabled",false);
                });

            });

            
            /* /1.2 Domicilios */

            /* 1.3 Contable */
            $('#acredita_reintegro').on('change', function(event){
                $inputs = $("[name='cbu_acredita_reintegro'],[name='id_banco_acredita_reintegro'],[name='tipo_cuenta_acredita_reintegro'],[name='nro_cuenta_acredita_reintegro'],[name='cuit_cuil_acredita_reintegro']");
                var disabled = !event.target.checked;
                $inputs.attr("disabled",disabled);
                if(disabled){
                    $("[name='cbu_acredita_reintegro']").val("");
                    $("[name='id_banco_acredita_reintegro']").val(0);
                    $("[name='nro_cuenta_acredita_reintegro']").val("");
                    $("[name='cuit_cuil_acredita_reintegro']").val("");
                    $("[name='tipo_cuenta_acredita_reintegro']").prop('checked', false);
                }
            });

            $('[name="adherido_debito_autom"]').on('change', function(event){
                $inputs = $("[name='cbu_debito_autom'],[name='id_banco_debito_autom'],[name='nro_cuenta_debito_autom'],[name='cuit_cuil_debito_autom']");
                
                var disabled = !event.target.checked;
                $inputs.attr("disabled",disabled);
                if(disabled){
                    $("[name='cbu_debito_autom']").val("");
                    $("[name='id_banco_debito_autom']").val(0);
                    $("[name='nro_cuenta_debito_autom']").val("");
                    $("[name='cuit_cuil_debito_autom']").val("");
                }
                
            });

            $('form#formContable').on('submit',function(event){
                event.preventDefault();
                $form = $(this);
                $buttons = $form.find("button");
                $inputs = $form.find("input:not([disabled])");
                
                var url = $form.attr("action");
                var data = $form.serialize();

                $buttons.attr("disabled",true);
                $inputs.attr("disabled",true);

                $.post(url, data)
                .done(function(res) {
                    console.log(res);
                    if(res.ok){
                        showSuccessToast("Los cambios han sido guardados.");
                    }
                    else{
                        if(res.errors.length > 0){
                            showErrorToast(res.errors[0]);
                        }
                        else{
                            showErrorToast("Lo sentimos, ha ocurrido un error.");
                        }
                    }
                })
                .fail(function(err) {
                    showErrorToast("Lo sentimos, ha ocurrido un error.");
                })
                .always(function(data) {
                    $buttons.attr("disabled",false);
                    $inputs.attr("disabled",false);
                });

            });
            /* /1.3 Contable */

            /* 1.4 Beneficiarios */
            $('#modalEliminarBeneficiario').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget).find("button");
                var id = button.data('id');
                var desc = button.data('text');

                var modal = $(this);
                modal.find(".text").text(desc);
                modal.find("button[name='eliminar']").data("id", id);
            });

            $('#modalEliminarBeneficiario').on("hide.bs.modal", function(){
                var modal = $(this);
                modal.find(".text").text("");
                modal.find("button[name='eliminar']").data("id","");
            });

            $("#modalEliminarBeneficiario button[name='eliminar']").on("click", function(){
                var button = $(this);
                var id = button.data("id");

                $buttons = $("#modalEliminarBeneficiario button");
                $buttons.attr("disabled",true);
                
                var data = { id : id};
                $.post('<?php echo base_url();?>afiliado/eliminarbeneficiario', data)
                .done(function(res) {
                    if(res.ok){
                        $("#modalEliminarBeneficiario").modal('hide');
                        location.reload();
                    }
                    else{
                        showErrorToast("Lo sentimos, ha ocurrido un error.");
                    }
                    
                })
                .fail(function(err) {
                    showErrorToast("Lo sentimos, ha ocurrido un error.");
                })
                .always(function(data) {
                    $buttons.attr("disabled",false);
                    $("#modalEliminarBeneficiario").modal('hide');
                });

            });

            $("button[name='nuevo-beneficiario']").on("click", function(){
                $("#tableBeneficiarios").find("button").attr("disabled",true);

                $("#formBeneficiarios").find("input,select,button").attr("disabled",false);
            });

            $("button[name='editar-beneficiario']").on("click", function(){
                $("#tableBeneficiarios").find("button").attr("disabled",true);
                
                $button = $(this);

                $("input#id_beneficiario_fallecimiento").val($button.data("id"));
                $("input#nombre_completo").val($button.data("nombre"));
                $("select#id_tipo_documento").val($button.data("idtipodoc"));
                $("input#nro_documento").val($button.data("nrodoc"));
                $("input#email").val($button.data("email"));
                $("input#telefono").val($button.data("telefono"));

                $("#formBeneficiarios").find("input,select,button").attr("disabled",false);
            });

            $('form#formBeneficiarios').on('submit',function(event){
                event.preventDefault();
                $form = $(this);
                $buttons = $form.find("button");
                $inputs = $form.find("input:not([disabled]),select:not([disabled])");
                
                var url = $form.attr("action");
                var data = $form.serialize();

                $buttons.attr("disabled",true);
                $inputs.attr("disabled",true);

                $.post(url, data)
                .done(function(res) {
                    if(res.ok){
                        location.reload();
                    }
                    else{
                        if(res.errors.length > 0){
                            showErrorToast(res.errors[0]);
                        }
                        else{
                            showErrorToast("Lo sentimos, ha ocurrido un error.");
                        }
                    }
                })
                .fail(function(err) {
                    showErrorToast("Lo sentimos, ha ocurrido un error.");
                })
                .always(function(data) {
                    $buttons.attr("disabled",false);
                    $inputs.attr("disabled",false);
                });

            });

            $('form#formBeneficiarios').on('reset',function(event){
                $("input[type='hidden']").val(undefined);
                $("#tableBeneficiarios").find("button").attr("disabled",false);
                $("#formBeneficiarios").find("input,select,button").attr("disabled",true);
            });

            /* /1.4 Beneficiarios */

            /* --------- /1. PERFIL AFILIADOS ----------- */

            /* --------- 2. PERFIL EMPRESA ----------- */

            /* 2.1 Datos generales */
            
            $('form#formEmpresaDatosGenerales').on('submit',function(event){
                event.preventDefault();
                $form = $(this);
                $buttons = $form.find("button");
                $inputs = $form.find("input:not([disabled])");
                
                var url = $form.attr("action");
                var data = $form.serialize();

                $buttons.attr("disabled",true);
                $inputs.attr("disabled",true);

                $.post(url, data)
                .done(function(res) {
                    console.log(res);
                    if(res.ok){
                        showSuccessToast("Los cambios han sido guardados.");
                    }
                    else{
                        if(res.errors.length > 0){
                            showErrorToast(res.errors[0]);
                        }
                        else{
                            showErrorToast("Lo sentimos, ha ocurrido un error.");
                        }
                    }
                })
                .fail(function(err) {
                    showErrorToast("Lo sentimos, ha ocurrido un error.");
                })
                .always(function(data) {
                    $buttons.attr("disabled",false);
                    $inputs.attr("disabled",false);
                });

            });
            /* /2.1 Datos generales */

            /* 2.2 Domicilios */            
            $('form#formEmpresaDomicilios').on('submit',function(event){
                event.preventDefault();
                $form = $(this);
                $buttons = $form.find("button");
                $inputs = $form.find("input:not([disabled]),select:not([disabled])");
                
                var url = $form.attr("action");
                var data = $form.serialize();

                $buttons.attr("disabled",true);
                $inputs.attr("disabled",true);

                $.post(url, data)
                .done(function(res) {
                    console.log(res);
                    if(res.ok){
                        showSuccessToast("Los cambios han sido guardados.");
                    }
                    else{
                        if(res.errors.length > 0){
                            showErrorToast(res.errors[0]);
                        }
                        else{
                            showErrorToast("Lo sentimos, ha ocurrido un error.");
                        }
                    }
                })
                .fail(function(err) {
                    showErrorToast("Lo sentimos, ha ocurrido un error.");
                })
                .always(function(data) {
                    $buttons.attr("disabled",false);
                    $inputs.attr("disabled",false);
                });

            });
            /* /2.2 Domicilios */

            /* --------- /2. PERFIL EMPRESA ----------- */

            /* --------- 3. PERFIL PRESTADOR ----------- */

            /* 3.1 Datos generales */
            
            $('form#formPrestadorDatosGenerales').on('submit',function(event){
                event.preventDefault();
                $form = $(this);
                $buttons = $form.find("button");
                $inputs = $form.find("input:not([disabled]),select:not([disabled])");
                
                var url = $form.attr("action");
                var data = $form.serialize();

                $buttons.attr("disabled",true);
                $inputs.attr("disabled",true);

                $.post(url, data)
                .done(function(res) {
                    console.log(res);
                    if(res.ok){
                        showSuccessToast("Los cambios han sido guardados.");
                    }
                    else{
                        if(res.errors.length > 0){
                            showErrorToast(res.errors[0]);
                        }
                        else{
                            showErrorToast("Lo sentimos, ha ocurrido un error.");
                        }
                    }
                })
                .fail(function(err) {
                    showErrorToast("Lo sentimos, ha ocurrido un error.");
                })
                .always(function(data) {
                    $buttons.attr("disabled",false);
                    $inputs.attr("disabled",false);
                });

            });
            /* /3.1 Datos generales */

            /* 3.2 Domicilios */            
            $('form#formPrestadorDomicilio').on('submit',function(event){
                event.preventDefault();
                $form = $(this);
                $buttons = $form.find("button");
                $inputs = $form.find("input:not([disabled]),select:not([disabled])");
                
                var url = $form.attr("action");
                var data = $form.serialize();

                $buttons.attr("disabled",true);
                $inputs.attr("disabled",true);

                $.post(url, data)
                .done(function(res) {
                    console.log(res);
                    if(res.ok){
                        showSuccessToast("Los cambios han sido guardados.");
                    }
                    else{
                        if(res.errors.length > 0){
                            showErrorToast(res.errors[0]);
                        }
                        else{
                            showErrorToast("Lo sentimos, ha ocurrido un error.");
                        }
                    }
                })
                .fail(function(err) {
                    showErrorToast("Lo sentimos, ha ocurrido un error.");
                })
                .always(function(data) {
                    $buttons.attr("disabled",false);
                    $inputs.attr("disabled",false);
                });

            });
            /* /3.2 Domicilios */

            /* --------- /3. PERFIL PRESTADOR ----------- */

        });
    </script>
</body>

</html>