<h4 class="text-primary">
    <small class="category"><?php echo($datosGenerales->razon_social) ?></small><br/>
    Datos Generales</h4>
<hr>

<form id="formEmpresaDatosGenerales" action="<?php echo base_url();?>empresa/actualizarDatosGenerales">
    <div class="row">
        <div class="form-group col-lg-3">
            <label for="fecha_alta" class="text-muted small">Fecha Alta</label>
            <input id="fecha_alta" name="fecha_alta" type="text" value="<?php echo($datosGenerales->fecha_alta) ?>" class="form-control" disabled>
        </div>
        <div class="form-group col-lg-3">
            <label for="razon_social" class="text-muted small">Razón Social</label>
            <input id="razon_social" name="razon_social" type="text" value="<?php echo($datosGenerales->razon_social) ?>" class="form-control" required>
        </div>    
        <div class="form-group col-lg-3">
            <label for="cuit" class="text-muted small">CUIT</label>
            <input id="cuit" name="cuit" type="text" value="<?php echo($datosGenerales->cuit) ?>" class="form-control" disabled>
        </div>
        <div class="col-lg-3">
            <div class="datepicker-container">
                <div class="form-group ">
                    <label for="fecha_inicio_actividad" class="text-muted small">Fecha Inicio Actividad</label>
                    <input id="fecha_inicio_actividad" name="fecha_inicio_actividad" type="text" class="form-control date-picker" value="<?php echo($datosGenerales->fecha_inicio_actividad) ?>" data-date-format="yyyy-mm-dd" data-datepicker-color="primary">
                </div>
            </div>
        </div>
        <div class="form-group col-lg-3">
            <label for="id_tipo_sociedad" class="text-muted small">Tipo de Sociedad</label>
            <select id="id_tipo_sociedad" name="id_tipo_sociedad" class="form-control">
                <?php foreach ($tiposSociedades as $key => $value) {  ?>
                    <option value="<?php echo $value->id_tipo_sociedad ?>" <?php if($value->id_tipo_sociedad == $datosGenerales->id_tipo_sociedad) echo "selected";?>><?php echo $value->desc_tipo_sociedad ?></option>
                <?php } ?> 
            </select>
        </div>
        <div class="form-group col-lg-3">
            <label for="id_rubro" class="text-muted small">Rubro</label>
            <select id="id_rubro" name="id_rubro" class="form-control">
                <?php foreach ($rubros as $key => $value) {  ?>
                    <option value="<?php echo $value->id_rubro ?>" <?php if($value->id_rubro == $datosGenerales->id_rubro) echo "selected";?>><?php echo $value->desc_rubro ?></option>
                <?php } ?> 
            </select>
        </div>
        <div class="form-group col-lg-3">
            <label for="cuenta" class="text-muted small">Cuenta</label>
            <input id="cuenta" name="cuenta" type="text" value="<?php echo($datosGenerales->cuenta) ?>" class="form-control">
        </div>     
    </div>
    <hr>
    <div class="row mt-4 mb-3">
        <div class="col-lg-12 text-right">
            <button type="submit" class="btn btn-primary">Guardar</button>
            <button type="button" class="btn btn-secondary reloadButton">Cancelar</button>
        </div>
    </div>
</form>