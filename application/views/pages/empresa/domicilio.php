<h4 class="text-primary">
    <small class="category">EMPRESA</small><br/>
    Domicilio</h4>
<hr>

<form id="formEmpresaDomicilios" action="<?php echo base_url();?>empresa/actualizarDomicilio">
    <div class="row">
        <?php if(isset($domicilioLegal)){ ?>

        <input type="hidden" name="domicilioLegal_id_domicilio" value="<?php echo $domicilioLegal->id_domicilio ?>"/>

        <div class="col-lg-6">
            <div class="category">Domicilio Empresa</div>
            <hr>
            <div class="row">
                <div class="form-group col-lg-12">
                    <label for="domicilioLegal_calle" class="text-muted small">Calle</label>
                    <input id="domicilioLegal_calle" name="domicilioLegal_calle" type="text" class="form-control" value="<?php echo($domicilioLegal->calle) ?>" required>
                </div>  
                
                <div class="form-group col-lg-4">
                    <label for="domicilioLegal_numero" class="text-muted small">Número</label>
                    <input id="domicilioLegal_numero" name="domicilioLegal_numero" type="text" class="form-control" value="<?php echo($domicilioLegal->numero) ?>" required>
                </div>
            
                <div class="form-group col-lg-4">
                    <label for="domicilioLegal_piso" class="text-muted small">Piso</label>
                    <input id="domicilioLegal_piso" name="domicilioLegal_piso" type="text" class="form-control" value="<?php echo($domicilioLegal->piso) ?>" required>
                </div>
            
                <div class="form-group col-lg-4">
                    <label for="domicilioLegal_depto" class="text-muted small">Dpto</label>
                    <input id="domicilioLegal_depto" name="domicilioLegal_depto" type="text" class="form-control" value="<?php echo($domicilioLegal->depto) ?>" required>
                </div>
            
                <div class="form-group col-lg-12">
                    <label for="domicilioLegal_referencia_adicional" class="text-muted small">Referencia adicional</label>
                    <input id="domicilioLegal_referencia_adicional" name="domicilioLegal_referencia_adicional" type="text" class="form-control" value="<?php echo($domicilioLegal->referencia_adicional) ?>">
                </div>
            
                <div class="form-group col-lg-5">
                    <label for="domicilioLegal_id_provincia" class="text-muted small">Provincia</label>
                    <select id="domicilioLegal_id_provincia" name="domicilioLegal_id_provincia" class="form-control" required data-localidad="domicilioLegal_desc_localidad">
                        <?php foreach ($provincias as $key => $value) {  ?>
                            <option value="<?php echo $value->id_provincia ?>" <?php if($value->id_provincia == $domicilioLegal->id_provincia) echo"selected";?>><?php echo $value->desc_provincia ?></option>
                        <?php } ?> 
                    </select>
                </div>
            
                <div class="form-group col-lg-7">
                    <label for="domicilioLegal_desc_localidad" class="text-muted small">Localidad y código postal</label>
                    <input id="domicilioLegal_desc_localidad" name="domicilioLegal_desc_localidad" class="localidades-typeahead form-control"  value="<?php if(!empty($domicilioLegal->codigo_postal)) echo '('.$domicilioLegal->codigo_postal.') '. $domicilioLegal->desc_localidad ?>"  required>
                    
                    <input type="hidden" id="domicilioLegal_id_localidad"  name="domicilioLegal_id_localidad" value="<?php echo $domicilioLegal->id_localidad ?>">
                    <!-- <input id="domicilioLegal_codigo_postal" name="domicilioLegal_codigo_postal" type="hidden" value="<?php echo($domicilioLegal->codigo_postal) ?>"> -->
                </div>
               

            </div>
            <hr>
        </div>
        
        <?php } ?>

        <?php if(isset($domicilioPostal)){ ?>
        
        <input type="hidden" name="domicilioPostal_id_domicilio" value="<?php echo $domicilioPostal->id_domicilio ?>"/>

        <div class="col-lg-6">
            <div class="category">Domicilio Facturación</div>
            <hr>
            <div class="row">
                <div class="form-group col-lg-12">
                    <label for="domicilioPostal_calle" class="text-muted small">Calle</label>
                    <input id="domicilioPostal_calle" name="domicilioPostal_calle" type="text" class="form-control" value="<?php echo($domicilioPostal->calle) ?>">
                </div>  
                
                <div class="form-group col-lg-4">
                    <label for="domicilioPostal_numero" class="text-muted small">Número</label>
                    <input id="domicilioPostal_numero" name="domicilioPostal_numero" type="text" class="form-control" value="<?php echo($domicilioPostal->numero) ?>">
                </div>
            
                <div class="form-group col-lg-4">
                    <label for="domicilioPostal_piso" class="text-muted small">Piso</label>
                    <input id="domicilioPostal_piso" name="domicilioPostal_piso" type="text" class="form-control" value="<?php echo($domicilioPostal->piso) ?>">
                </div>
            
                <div class="form-group col-lg-4">
                    <label for="domicilioPostal_depto" class="text-muted small">Dpto</label>
                    <input id="domicilioPostal_depto" name="domicilioPostal_depto" type="text" class="form-control" value="<?php echo($domicilioPostal->depto) ?>">
                </div>
            
                <div class="form-group col-lg-12">
                    <label for="domicilioPostal_referencia_adicional" class="text-muted small">Referencia adicional</label>
                    <input id="domicilioPostal_referencia_adicional" name="domicilioPostal_referencia_adicional" type="text" class="form-control" value="<?php echo($domicilioPostal->referencia_adicional) ?>">
                </div>

                <div class="form-group col-lg-5">
                    <label for="domicilioPostal_id_provincia" class="text-muted small">Provincia</label>
                    <select id="domicilioPostal_id_provincia" name="domicilioPostal_id_provincia" class="form-control" required data-localidad="domicilioPostal_desc_localidad">
                        <?php foreach ($provincias as $key => $value) {  ?>
                            <option value="<?php echo $value->id_provincia ?>" <?php if($value->id_provincia == $domicilioPostal->id_provincia) echo"selected";?>><?php echo $value->desc_provincia ?></option>
                        <?php } ?> 
                    </select>
                </div>
            
                <div class="form-group col-lg-7">
                    <label for="domicilioPostal_desc_localidad" class="text-muted small">Localidad y código postal</label>
                    <input id="domicilioPostal_desc_localidad" name="domicilioPostal_desc_localidad" class="localidades-typeahead form-control"  value="<?php if(!empty($domicilioPostal->codigo_postal)) echo '('.$domicilioPostal->codigo_postal.') '. $domicilioPostal->desc_localidad ?>">
                    
                    <input type="hidden" id="domicilioPostal_id_localidad"  name="domicilioPostal_id_localidad" value="<?php echo $domicilioPostal->id_localidad ?>">
                    <!-- <input id="domicilioPostal_codigo_postal" name="domicilioPostal_codigo_postal" type="hidden" value="<?php echo($domicilioPostal->codigo_postal) ?>"> -->
                </div>

                
            </div>
            <hr>
        </div>

        <?php } ?>

        <?php if(isset($datosContacto)){ ?>
        
        <div class="col-lg-12">
            <div class="category">Datos de contacto</div>
            <hr>
            <div class="row">
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="telefono_fijo" class="text-muted small">Teléfono</label>
                        <input id="telefono_fijo" name="telefono_fijo" type="tel" class="form-control" value="<?php echo($datosContacto->telefono_fijo) ?>">
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="email1" class="text-muted small">E-mail</label>
                        <input id="email1" name="email1" type="email" class="form-control" value="<?php echo($datosContacto->email1) ?>">
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="fax" class="text-muted small">FAX</label>
                        <input id="fax" name="fax" type="text" class="form-control" value="<?php echo($datosContacto->fax) ?>">
                    </div>
                </div>  
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="telefono_movil" class="text-muted small">Teléfono alternativo</label>
                        <input id="telefono_movil" name="telefono_movil" type="text" class="form-control" value="<?php echo($datosContacto->telefono_movil) ?>">
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="email2" class="text-muted small">E-mail alternativo</label>
                        <input id="email2" name="email2" type="email" class="form-control" value="<?php echo($datosContacto->email2) ?>">
                    </div>
                </div>             
            </div>
        </div>

        <?php } ?>
    </div>
    <hr>
    <div class="row mt-4 mb-3">
        <div class="col-lg-12 text-right">
            <button type="submit" class="btn btn-primary">Guardar</button>
            <button type="reset" class="btn btn-secondary reloadButton">Cancelar</button>
        </div>
    </div>
</form>