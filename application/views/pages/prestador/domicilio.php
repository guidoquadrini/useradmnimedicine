<h4 class="text-primary">
    <small class="category">PRESTADOR</small><br/>
    Domicilio</h4>
<hr>

<form id="formPrestadorDomicilio" action="<?php echo base_url();?>prestador/actualizarDomicilio">
    <div class="row">
        <?php if(isset($domicilio)){ ?>

        <input type="hidden" name="domicilioLegal_id_domicilio" value="<?php echo $domicilio->id_domicilio ?>"/>

        <div class="col-lg-12">
            <div class="category">Domicilio Prestador</div>
            <hr>
            <div class="row">
                <div class="form-group col-lg-6">
                    <label for="domicilioLegal_calle" class="text-muted small">Calle</label>
                    <input id="domicilioLegal_calle" name="domicilioLegal_calle" type="text" class="form-control" value="<?php echo($domicilio->calle) ?>" required>
                </div>  
                
                <div class="form-group col-lg-2">
                    <label for="domicilioLegal_numero" class="text-muted small">Número</label>
                    <input id="domicilioLegal_numero" name="domicilioLegal_numero" type="text" class="form-control" value="<?php echo($domicilio->numero) ?>" required>
                </div>
            
                <div class="form-group col-lg-2">
                    <label for="domicilioLegal_piso" class="text-muted small">Piso</label>
                    <input id="domicilioLegal_piso" name="domicilioLegal_piso" type="text" class="form-control" value="<?php echo($domicilio->piso) ?>" required>
                </div>
            
                <div class="form-group col-lg-2">
                    <label for="domicilioLegal_depto" class="text-muted small">Dpto</label>
                    <input id="domicilioLegal_depto" name="domicilioLegal_depto" type="text" class="form-control" value="<?php echo($domicilio->depto) ?>" required>
                </div>
            
                <div class="form-group col-lg-6">
                    <label for="domicilioLegal_referencia_adicional" class="text-muted small">Referencia adicional</label>
                    <input id="domicilioLegal_referencia_adicional" name="domicilioLegal_referencia_adicional" type="text" class="form-control" value="<?php echo($domicilio->referencia_adicional) ?>">
                </div>
            
                <div class="form-group col-lg-2">
                    <label for="domicilioLegal_id_provincia" class="text-muted small">Provincia</label>
                    <select id="domicilioLegal_id_provincia" name="domicilioLegal_id_provincia" class="form-control" required data-localidad="domicilioLegal_desc_localidad" required>
                        <?php foreach ($provincias as $key => $value) {  ?>
                            <option value="<?php echo $value->id_provincia ?>" <?php if($value->id_provincia == $domicilio->id_provincia) echo"selected";?>><?php echo $value->desc_provincia ?></option>
                        <?php } ?> 
                    </select>
                </div>
            
                <div class="form-group col-lg-4">
                    <label for="domicilioLegal_desc_localidad" class="text-muted small">Localidad y código postal</label>
                    <input id="domicilioLegal_desc_localidad" name="domicilioLegal_desc_localidad" class="localidades-typeahead form-control"  value="<?php if(!empty($domicilio->codigo_postal)) echo '('.$domicilio->codigo_postal.') '. $domicilio->desc_localidad ?>" required>
                    
                    <input type="hidden" id="domicilioLegal_id_localidad"  name="domicilioLegal_id_localidad" value="<?php echo $domicilio->id_localidad ?>">
                    <!-- <input id="domicilioLegal_codigo_postal" name="domicilioLegal_codigo_postal" type="hidden" value="<?php echo($domicilio->codigo_postal) ?>"> -->
                </div>
               

            </div>
            <hr>
        </div>
        
        <?php } ?>

        
        <div class="col-lg-12">
            <div class="category">Datos de contacto</div>
            <hr>
            <div class="row">
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="telefono_fijo" class="text-muted small">Teléfono</label>
                        <input id="telefono_fijo" name="telefono_fijo" type="tel" class="form-control" value="<?php echo($domicilio->telefono_fijo) ?>">
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="email1" class="text-muted small">E-mail</label>
                        <input id="email1" name="email1" type="email" class="form-control" value="<?php echo($domicilio->email1) ?>">
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="fax" class="text-muted small">FAX</label>
                        <input id="fax" name="fax" type="text" class="form-control" value="<?php echo($domicilio->fax) ?>">
                    </div>
                </div>  
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="telefono_movil" class="text-muted small">Teléfono alternativo</label>
                        <input id="telefono_movil" name="telefono_movil" type="text" class="form-control" value="<?php echo($domicilio->telefono_movil) ?>">
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="email2" class="text-muted small">E-mail alternativo</label>
                        <input id="email2" name="email2" type="email" class="form-control" value="<?php echo($domicilio->email2) ?>">
                    </div>
                </div>             
            </div>
        </div>

    </div>
    <hr>
    <div class="row mt-4 mb-3">
        <div class="col-lg-12 text-right">
            <button type="submit" class="btn btn-primary">Guardar</button>
            <button type="reset" class="btn btn-secondary reloadButton">Cancelar</button>
        </div>
    </div>
</form>