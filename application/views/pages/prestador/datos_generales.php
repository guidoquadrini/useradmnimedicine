<h4 class="text-primary">
    <small class="category"><?php echo($datosGenerales->nombre_razon_social) ?></small><br/>
    Datos Generales</h4>
<hr>

<form id="formPrestadorDatosGenerales" action="<?php echo base_url();?>prestador/actualizarDatosGenerales">
    <div class="row">
        <div class="form-group col-lg-3">
            <label for="id_titulo_abreviado" class="text-muted small">Título</label>
            <select id="id_titulo_abreviado" name="id_titulo_abreviado" class="form-control">
                <?php foreach ($titulos as $key => $value) {  ?>
                    <option value="<?php echo $value->id_titulo_abreviado ?>" <?php if($value->id_titulo_abreviado == $datosGenerales->id_titulo_abreviado) echo "selected";?>><?php echo $value->desc_titulo_abreviado ?></option>
                <?php } ?> 
            </select>
        </div>
        <div class="form-group col-lg-3">
            <label for="nombre_razon_social" class="text-muted small">Nombre completo/Razón social</label>
            <input id="nombre_razon_social" name="nombre_razon_social" type="text" value="<?php echo($datosGenerales->nombre_razon_social) ?>" class="form-control" required>
        </div> 
        <div class="form-group col-lg-3">
            <label for="fecha_alta" class="text-muted small">Fecha Alta</label>
            <input id="fecha_alta" name="fecha_alta" type="text" value="<?php echo($datosGenerales->fecha_alta) ?>" class="form-control" disabled>
        </div>
        <div class="form-group col-lg-3">
            <label for="documento" class="text-muted small">Tipo y Nro de Documento</label>
            <input id="documento" name="documento" type="text" value="<?php echo($datosGenerales->desc_tipo_documento) ?> <?php echo($datosGenerales->nro_documento) ?>" class="form-control" disabled>
        </div>
        <div class="col-lg-3">
            <div class="datepicker-container">
                <div class="form-group ">
                    <label for="fecha_nacimiento" class="text-muted small">Fecha de nacimiento</label>
                    <input id="fecha_nacimiento" name="fecha_nacimiento" type="text" class="form-control date-picker" value="<?php echo($datosGenerales->fecha_nacimiento) ?>" data-date-format="yyyy-mm-dd" data-datepicker-color="primary">
                </div>
            </div>
        </div>
        <div class="form-group col-lg-3">
            <label for="cuil_cuit" class="text-muted small">CUIT/CUIL</label>
            <input id="cuil_cuit" name="cuil_cuit" type="text" value="<?php echo($datosGenerales->cuil_cuit) ?>" class="form-control" disabled>
        </div> 
        <div class="form-group col-lg-3">
            <label for="inos_anssal" class="text-muted small">INOS/ANSSAL</label>
            <input id="inos_anssal" name="inos_anssal" type="text" value="<?php echo($datosGenerales->inos_anssal) ?>" class="form-control">
        </div> 
        <div class="col-lg-3">
            <div class="datepicker-container">
                <div class="form-group ">
                    <label for="fecha_inicio_actividad" class="text-muted small">Fecha Inicio Actividades</label>
                    <input id="fecha_inicio_actividad" name="fecha_inicio_actividad" type="text" class="form-control date-picker" value="<?php echo($datosGenerales->fecha_inicio_actividad) ?>" data-date-format="yyyy-mm-dd" data-datepicker-color="primary">
                </div>
            </div>
        </div>
        <div class="form-group col-lg-3">
            <label for="matricula_provincial" class="text-muted small">Matricula provincial</label>
            <input id="matricula_provincial" name="matricula_provincial" type="text" value="<?php echo($datosGenerales->matricula_provincial) ?>" class="form-control">
        </div> 
        <div class="form-group col-lg-3">
            <label for="matricula_nacional" class="text-muted small">Matricula nacional</label>
            <input id="matricula_nacional" name="matricula_nacional" type="text" value="<?php echo($datosGenerales->matricula_nacional) ?>" class="form-control">
        </div> 
        <div class="form-group col-lg-3">
            <label for="circulo" class="text-muted small">Círculo</label>
            <div class="form-check mt-1">
                <label class="form-check-label">
                    <input class="form-check-input" type="checkbox" id="circulo" name="circulo" <?php if($datosGenerales->circulo == 1) echo "checked";?>>
                    <span class="form-check-sign"></span>
                    &nbsp;
                </label>
            </div>
        </div>
        
        <div class="form-group col-lg-3">
            <label for="fecha_desde_suspension" class="text-muted small">Fecha de suspensión (desde)</label>
            <input id="fecha_desde_suspension" name="fecha_desde_suspension" type="text" value="<?php echo($datosGenerales->fecha_desde_suspension) ?>" class="form-control" disabled>
        </div> 
        <div class="form-group col-lg-3">
            <label for="fecha_hasta_suspension" class="text-muted small">Fecha de suspensión (hasta)</label>
            <input id="fecha_hasta_suspension" name="fecha_hasta_suspension" type="text" value="<?php echo($datosGenerales->fecha_hasta_suspension) ?>" class="form-control" disabled>
        </div>
        <div class="form-group col-lg-3">
            <label for="desc_prestador_motivo_suspension" class="text-muted small">Motivo de suspensión</label>
            <input id="desc_prestador_motivo_suspension" name="desc_prestador_motivo_suspension" type="text" value="<?php echo($datosGenerales->desc_prestador_motivo_suspension) ?>" class="form-control" disabled>
        </div>    
    </div>
    <hr>
    <div class="row mt-4 mb-3">
        <div class="col-lg-12 text-right">
            <button type="submit" class="btn btn-primary">Guardar</button>
            <button type="button" class="btn btn-secondary reloadButton">Cancelar</button>
        </div>
    </div>
</form>