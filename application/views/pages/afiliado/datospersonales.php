<h4 class="text-primary">
    <small class="category">AFILIADO</small><br/>
    Datos Personales</h4>
<hr>

<form id="formDatosPersonales" action="<?php echo base_url();?>afiliado/actualizarDatosPersonales">
    <div class="row">
        <div class="form-group col-lg-3">
            <label for="fecha_ingreso" class="text-muted small">Fecha Ingreso</label>
            <input id="fecha_ingreso" name="fecha_ingreso" type="text" value="<?php echo($datospersonales->fecha_ingreso) ?>" class="form-control" disabled>
        </div>
        <div class="form-group col-lg-3">
            <label for="apellido_nombre" class="text-muted small">Apellido y nombre</label>
            <input id="apellido_nombre" name="apellido_nombre" type="text" value="<?php echo($datospersonales->apellido_nombre) ?>" class="form-control" disabled>
        </div>    
        <div class="form-group col-lg-3">
            <label for="desc_sexo" class="text-muted small">Sexo</label>
            <input id="desc_sexo" name="desc_sexo" type="text" value="<?php echo($datospersonales->desc_sexo) ?>" class="form-control" disabled>
        </div>
        <div class="form-group col-lg-3">
            <label for="documento" class="text-muted small">Tipo y Nro de Documento</label>
            <input id="documento" name="documento" type="text" value="<?php echo($datospersonales->desc_tipo_documento) ?> <?php echo($datospersonales->nro_documento) ?>" class="form-control" disabled>
        </div>
        <div class="form-group col-lg-3">
            <label for="cuil_cuit" class="text-muted small">CUIT / CUIL</label>
            <input id="cuil_cuit" name="cuil_cuit" type="text" value="<?php echo($datospersonales->cuil_cuit) ?>" class="form-control" disabled>
        </div>
        <div class="form-group col-lg-3">
            <label for="fecha_nacimiento" class="text-muted small">Fecha Nacimiento</label>
            <input id="fecha_nacimiento" name="fecha_nacimiento" type="text" value="<?php echo($datospersonales->fecha_nacimiento) ?>" class="form-control" disabled>
        </div>
        <div class="form-group col-lg-3">
            <label for="edad" class="text-muted small">Edad</label>
            <input id="edad" name="edad" type="text" value="<?php echo($datospersonales->edad) ?>" class="form-control" disabled>
        </div>
        <div class="form-group col-lg-3">
            <label for="id_cargo" class="text-muted small">Cargo</label>
            <select id="id_cargo" name="id_cargo" class="form-control" required>
                <?php foreach ($cargos as $key => $value) {  ?>
                    <option value="<?php echo $value->id_cargo ?>" <?php if($value->id_cargo == $datospersonales->id_cargo) echo"selected";?>><?php echo $value->desc_cargo ?></option>
                <?php } ?> 
            </select>
        </div>
        <div class="form-group col-lg-3">
            <label for="desc_nacionalidad" class="text-muted small">Nacionalidad</label>
            <input id="desc_nacionalidad" name="desc_nacionalidad" type="text" value="<?php echo($datospersonales->desc_nacionalidad) ?>" class="form-control" disabled>
        </div>
        <div class="form-group col-lg-3">
            <label for="desc_estado_civil" class="text-muted small">Estado Civil</label>
            <input id="desc_estado_civil" name="desc_estado_civil" type="text" value="<?php echo($datospersonales->desc_estado_civil) ?>" class="form-control" disabled>
        </div>
        <div class="form-group col-lg-3">
            <label for="tipo_grupo" class="text-muted small">Tipo Grupo</label>
            <input id="tipo_grupo" name="tipo_grupo" type="text" value="<?php echo($datospersonales->tipo_grupo) ?>" class="form-control" disabled>
        </div>
        <div class="form-group col-lg-3">
            <label for="prestadora_nombre_corto" class="text-muted small">Prestadora</label>
            <input id="prestadora_nombre_corto" name="prestadora_nombre_corto" type="text" value="<?php echo($datospersonales->prestadora_nombre_corto) ?>" class="form-control" disabled>
        </div>
        <div class="form-group col-lg-3">
            <label for="plan_nombre_corto" class="text-muted small">Plan</label>
            <input id="plan_nombre_corto" name="plan_nombre_corto" type="text" value="<?php echo($datospersonales->plan_nombre_corto) ?>" class="form-control" disabled>
        </div>
        <!-- TODO: Consultar plan materno -->
        <!-- <div class="form-group col-lg-4">
            <label for="valor_corporativo" class="text-muted small">Plan materno</label>
            <div class="form-check mt-1">
                <label class="form-check-label">
                    <input class="form-check-input" type="checkbox" id="valor_corporativo" disabled>
                    <span class="form-check-sign"></span>
                    &nbsp;
                </label>
            </div>
        </div> -->
    </div>
    <hr>
    <div class="row mt-4 mb-3">
        <div class="col-lg-12 text-right">
            <button type="submit" class="btn btn-primary">Guardar</button>
            <button type="button" class="btn btn-secondary reloadButton">Cancelar</button>
        </div>
    </div>
</form>