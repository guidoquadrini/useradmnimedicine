<h4 class="text-primary">
    <small class="category">AFILIADO</small><br/>
    Contable</h4>
<hr>

<form id="formContable" action="<?php echo base_url();?>afiliado/actualizarContable">
    <div class="row">
        <div class="form-group col-lg-4">
            <label for="desc_condicion_fiscal" class="text-muted small">Condición Fiscal</label>
            <input id="desc_condicion_fiscal" name="desc_condicion_fiscal" type="text" class="form-control" value="<?php echo($contable->desc_condicion_fiscal) ?>" disabled>
        </div>  
        
        <div class="form-group col-lg-4">
            <label for="comprobante" class="text-muted small">Comprobante</label>
            <input id="comprobante" name="comprobante" type="text" class="form-control" value="<?php echo($contable->comprobante) ?>" disabled>
        </div>

        <div class="w-100"></div>
    
        <div class="form-group col-lg-4">
            <label for="desc_condicion_ganancia" class="text-muted small">Condición Ganancias</label>
            <input id="desc_condicion_ganancia" name="desc_condicion_ganancia" type="text" class="form-control" value="<?php echo($contable->desc_condicion_ganancia) ?>" disabled>
        </div>
    
        <div class="form-group col-lg-4">
            <label for="desc_condicion_iibb" class="text-muted small">Condición IIBB</label>
            <input id="desc_condicion_iibb" name="desc_condicion_iibb" type="text" class="form-control" value="<?php echo($contable->desc_condicion_iibb) ?>" disabled>
        </div>
    
        <div class="form-group col-lg-4">
            <label for="desc_condicion_iva" class="text-muted small">Condición IVA</label>
            <input id="desc_condicion_iva" name="desc_condicion_iva" type="text" class="form-control" value="<?php echo($contable->desc_condicion_iva) ?>" disabled>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="form-group col-lg-4">
            <label for="facturar_a" class="text-muted small">Facturar a</label>
            <input id="facturar_a" name="facturar_a" type="text" class="form-control" value="<?php echo($contable->facturar_a) ?>" disabled>
        </div>  
        
        <div class="form-group col-lg-4">
            <label for="valor_corporativo" class="text-muted small">Valor corporativo</label>
            <div class="form-check mt-1">
                <label class="form-check-label">
                    <input class="form-check-input" type="checkbox" id="valor_corporativo" <?php if($contable->valor_corporativo == 1) echo "checked";?> disabled>
                    <span class="form-check-sign"></span>
                    &nbsp;
                </label>
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="form-group col-lg-4">
            <label for="empresa_cargo" class="text-muted small">Empresa cargo</label>
            <input id="empresa_cargo" name="empresa_cargo" type="text" class="form-control" value="<?php echo($contable->empresa_cargo) ?>" disabled>
        </div>  
        
        <div class="form-group col-lg-4">
            <label for="vigencia_cobertura" class="text-muted small">Vigencia cobertura</label>
            <input id="vigencia_cobertura" name="vigencia_cobertura" type="text" class="form-control" value="<?php echo($contable->vigencia_cobertura) ?>" disabled>
        </div>

        <div class="w-100"></div>
        
        <div class="form-group col-lg-4">
            <label for="empresa_facturar" class="text-muted small">Empresa facturar</label>
            <input id="empresa_facturar" name="empresa_facturar" type="text" class="form-control" value="<?php echo($contable->empresa_facturar) ?>" disabled>
        </div>

        <div class="form-group col-lg-4">
            <label for="cuit_empresa_facturar" class="text-muted small">CUIT empresa facturar</label>
            <input id="cuit_empresa_facturar" name="cuit_empresa_facturar" type="text" class="form-control" value="<?php echo($contable->cuit_empresa_facturar) ?>" disabled>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="form-group col-lg-4 mb-4">
            <!-- <label for="acredita_reintegro" class="text-muted small">Acredita reintegro</label>
            <div class="form-check mt-1">
                <label class="form-check-label">
                    <input class="form-check-input" type="checkbox" id="acredita_reintegro">
                    <span class="form-check-sign"></span>
                    &nbsp;
                </label>
            </div> -->
            <div class="form-check">
                <label class="form-check-label">
                    <input id="acredita_reintegro" name="acredita_reintegro" class="form-check-input" type="checkbox" <?php if($contable->acredita_reintegro == 1) echo "checked";?>>
                    <span class="form-check-sign"></span>
                    Acredita reintegro
                </label>
            </div>
        </div>

        <div class="w-100"></div>

        <div class="form-group col-lg-4">
            <label for="cbu_acredita_reintegro" class="text-muted small">CBU</label>
            <input id="cbu_acredita_reintegro" name="cbu_acredita_reintegro" type="text" value="<?php echo($contable->cbu_acredita_reintegro) ?>" class="form-control" <?php if($contable->acredita_reintegro != 1) echo "disabled";?>>
        </div>  
        
        <div class="form-group col-lg-4">
            <label for="id_banco_acredita_reintegro" class="text-muted small">Banco</label>
            <select id="id_banco_acredita_reintegro" name="id_banco_acredita_reintegro" class="form-control" <?php if($contable->acredita_reintegro != 1) echo "disabled";?>>
                <?php foreach ($bancos as $key => $value) {  ?>
                    <option value="<?php echo $value->id_banco ?>" <?php if($value->id_banco == $contable->id_banco_acredita_reintegro) echo"selected";?>><?php echo $value->desc_banco ?></option>
                <?php } ?> 
            </select>
        </div>

        <div class="w-100"></div>

        <div class="form-group col-lg-4">
            <label class="text-muted small">Tipo Cuenta</label>

            <div class="form-check form-check-radio">
                <label class="form-check-label">
                    <input class="form-check-input" type="radio" name="tipo_cuenta_acredita_reintegro" 
                        value="caja ahorro" <?php if($contable->tipo_cuenta_acredita_reintegro == "caja ahorro") echo "checked";?> <?php if($contable->acredita_reintegro != 1) echo "disabled";?>>
                    <span class="form-check-sign"></span>
                    Caja de Ahorro
                </label>
            </div>

            <div class="form-check form-check-radio">
                <label class="form-check-label">
                    <input class="form-check-input" type="radio" name="tipo_cuenta_acredita_reintegro" 
                        value="cuenta corriente" <?php if($contable->tipo_cuenta_acredita_reintegro == "cuenta corriente") echo "checked";?> <?php if($contable->acredita_reintegro != 1) echo "disabled";?>>
                    <span class="form-check-sign"></span>
                    Cuenta Corriente
                </label>
            </div>
        </div>

        <div class="form-group col-lg-4">
            <label for="nro_cuenta_acredita_reintegro" class="text-muted small">Número de cuenta</label>
            <input id="nro_cuenta_acredita_reintegro" name="nro_cuenta_acredita_reintegro" type="text" value="<?php echo($contable->nro_cuenta_acredita_reintegro) ?>" class="form-control" <?php if($contable->acredita_reintegro != 1) echo "disabled";?>>
        </div> 

        <div class="form-group col-lg-4">
            <label for="cuit_cuil_acredita_reintegro" class="text-muted small">CUIT/CUIL</label>
            <input id="cuit_cuil_acredita_reintegro" name="cuit_cuil_acredita_reintegro" type="text" value="<?php echo($contable->cuit_cuil_acredita_reintegro) ?>" class="form-control" <?php if($contable->acredita_reintegro != 1) echo "disabled";?>>
        </div> 

    </div>
    <hr>
    <div class="row">

        <div class="form-group col-lg-4 mb-4">
            <div class="form-check">
                <label class="form-check-label">
                    <input name="adherido_debito_autom" class="form-check-input" type="checkbox" <?php if($contable->adherido_debito_autom == 1) echo "checked";?>>
                    <span class="form-check-sign"></span>
                    Acredita débito automático
                </label>
            </div>
        </div>

        <div class="w-100"></div>

        <div class="form-group col-lg-4">
            <label for="cbu_debito_autom" class="text-muted small">CBU</label>
            <input id="cbu_debito_autom" name="cbu_debito_autom" type="text" value="<?php echo($contable->cbu_debito_autom) ?>" class="form-control" <?php if($contable->adherido_debito_autom != 1) echo "disabled";?>>
        </div>  

        <div class="form-group col-lg-4">
            <label for="id_banco_debito_autom" class="text-muted small">Banco</label>
            <select id="id_banco_debito_autom" name="id_banco_debito_autom" class="form-control"  <?php if($contable->adherido_debito_autom != 1) echo "disabled";?>>
                <?php foreach ($bancos as $key => $value) {  ?>
                    <option value="<?php echo $value->id_banco ?>" <?php if($value->id_banco == $contable->id_banco_debito_autom) echo"selected";?>><?php echo $value->desc_banco ?></option>
                <?php } ?> 
            </select>
        </div>

        <div class="form-group col-lg-4">
            <label for="nro_cuenta_debito_autom" class="text-muted small">Número de cuenta</label>
            <input id="nro_cuenta_debito_autom" name="nro_cuenta_debito_autom" type="text" value="<?php echo($contable->nro_cuenta_debito_autom) ?>" class="form-control" <?php if($contable->adherido_debito_autom != 1) echo "disabled";?>>
        </div> 

        <div class="form-group col-lg-4">
            <label for="cuit_cuil_debito_autom" class="text-muted small">CUIT/CUIL</label>
            <input id="cuit_cuil_debito_autom" name="cuit_cuil_debito_autom" type="text" value="<?php echo($contable->cuit_cuil_debito_autom) ?>" class="form-control" <?php if($contable->adherido_debito_autom != 1) echo "disabled";?>>
        </div> 
    </div>
    <hr>
    <div class="row">
        <div class="form-group col-lg-4">
            <label for="cheque_orden_de" class="text-muted small">Cheque a la orden de</label>
            <input id="cheque_orden_de" name="cheque_orden_de" type="text" value="<?php echo($contable->cheque_orden_de) ?>" class="form-control" disabled>
        </div> 

        <div class="form-group col-lg-4">
            <label for="domicilio_cobro" class="text-muted small">Domicilio de cobro</label>
            <input id="domicilio_cobro" name="domicilio_cobro" type="text" value="<?php echo($contable->domicilio_cobro) ?>" class="form-control" disabled>
        </div>  

    </div>
    <hr>
    <div class="row mt-4 mb-3">
        <div class="col-lg-12 text-right">
            <button type="submit" class="btn btn-primary">Guardar</button>
            <button type="button" class="btn btn-secondary reloadButton">Cancelar</button>
        </div>
    </div>
</form>