<h4 class="text-primary">
    <small class="category">AFILIADO</small><br/>
    Grupo
</h4>

<div class="table-responsive mb-3">
    <table class="table table-striped mb-0">
        <thead>
            <tr>
            <th scope="col">N° Afiliado</th>
            <th scope="col">Nombre y Apellido</th>
            <th scope="col">Tipo y N° Documento</th>
            <th scope="col">CUIT</th>
            <th scope="col">Parentesco</th>
            <th scope="col">Edad</th>
            <th scope="col">Estado</th>
            </tr>
        </thead>
        <tbody>
            
            <?php foreach ($grupo as $key => $value) { ?>
                <tr>
                    <td><?php echo $value->nro_afiliado ?></td>
                    <td><?php echo $value->nombre_completo ?></td>
                    <td><?php echo $value->desc_tipo_documento ?> <?php echo $value->nro_documento ?></td>
                    <td><?php echo $value->cuil_cuit ?></td>
                    <td><?php echo $value->desc_parentesco ?></td>
                    <td><?php echo $value->edad ?></td>
                    <td><span class="badge <?php if($value->id_estado == 1) echo 'badge-info'; else echo 'badge-warning'; ?> "><?php echo $value->desc_estado ?></span></td>
                </tr> 
            <?php } ?>
                                   
        </tbody>
    </table>
</div>