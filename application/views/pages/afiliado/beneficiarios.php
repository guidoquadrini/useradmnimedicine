<h4 class="text-primary">
    <small class="category">AFILIADO</small><br/>
    Beneficiarios</h4>
<hr>

<form id="formBeneficiarios"  action="<?php echo base_url();?>afiliado/guardarBeneficiario">
    <input type="hidden" name="id_beneficiario_fallecimiento" id="id_beneficiario_fallecimiento"/>
    <div class="row">
        <div class="form-group col-lg-4">
            <label for="nombre_completo" class="text-muted small">Nombre y apellido</label>
            <input id="nombre_completo" name="nombre_completo" type="text" class="form-control" 
            required disabled maxlength="30">
        </div>

        <div class="form-group col-lg-4">
            <label for="id_tipo_documento" class="text-muted small">Tipo Documento</label>
            <select id="id_tipo_documento" name="id_tipo_documento" class="form-control" required disabled>
            <option value="">Seleccione un tipo de documento</option>    
            <?php foreach ($tiposDocumentos as $key => $value) {  ?>
                    <option value="<?php echo $value->id_tipo_documento ?>"><?php echo $value->desc_tipo_documento ?></option>
                <?php } ?> 
            </select>
        </div>

        <div class="form-group col-lg-4">
            <label for="nro_documento" class="text-muted small">N° Documento</label>
            <input id="nro_documento" name="nro_documento" type="text" class="form-control" 
            required disabled maxlength="8">
        </div>

        <div class="form-group col-lg-4">
            <label for="telefono" class="text-muted small">Teléfono</label>
            <input id="telefono" name="telefono" type="tel" class="form-control" 
            disabled maxlength="20">
        </div>

        <div class="form-group col-lg-4">
            <label for="email" class="text-muted small">E-mail</label>
            <input id="email" name="email" type="email" class="form-control" 
            disabled maxlength="255">
        </div>

    </div>
    <hr>
    <div class="row mt-4 mb-3">
        <div class="col-lg-12 text-right">
            <button type="submit" class="btn btn-primary" disabled>Guardar</button>
            <button type="reset" class="btn btn-secondary" disabled>Cancelar</button>
        </div>
    </div>
</form>

<div class="table-responsive my-4">
    <table class="table table-striped mb-0" id="tableBeneficiarios">
        <thead>
            <tr>
                <th scope="col">Nombre completo</th>
                <th scope="col">Tipo Documento</th>
                <th scope="col">N° Documento</th>
                <th scope="col">Teléfono</th>
                <th scope="col">E-mail</th>
                <th scope="col">
                    <div class="d-flex">
                        <button class="btn btn-primary btn-icon btn-round btn-tooltip my-0 btn-sm" type="button"
                            name="nuevo-beneficiario">
                                <i class="fa fa-plus"></i>
                        </button>
                    </div>
                </th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($beneficiarios as $key => $value) { ?>
            <tr>
                <td><?php echo $value->nombre_completo ?></td>
                <td><?php echo $value->desc_tipo_documento ?></td>
                <td><?php echo $value->nro_documento ?></td>
                <td><?php echo $value->telefono ?></td>
                <td><?php echo $value->email ?></td>
                <td>
                    <div class="d-flex">
                        <button class="btn btn-primary btn-icon btn-round my-0 mr-2 btn-sm" type="button"
                            name="editar-beneficiario"
                            data-id = "<?php echo $value->id_beneficiario_fallecimiento ?>"
                            data-nombre = "<?php echo $value->nombre_completo ?>"
                            data-idtipodoc = "<?php echo $value->id_tipo_documento ?>"
                            data-nrodoc = "<?php echo $value->nro_documento ?>"
                            data-telefono = "<?php echo $value->telefono ?>"
                            data-email = "<?php echo $value->email ?>">
                            <i class="fa fa-pen"></i>
                        </button>
                        <span data-toggle="modal" data-target="#modalEliminarBeneficiario">
                            <button class="btn btn-primary btn-icon btn-round my-0  btn-sm" type="button"
                                data-id = "<?php echo $value->id_beneficiario_fallecimiento ?>" data-text = "<?php echo $value->nombre_completo ?>">
                                <i class="fa fa-trash"></i>
                            </button>
                        </span>
                    </div>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>