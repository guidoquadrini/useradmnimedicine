var timer_token = setTimeout(function () { $("#campo_token").focus(); }, 1000);;
var estado_timer = true;
$(document).ready(function () {
    $("#campo_token").focus();

    /*!
    Sector de control de alertas
    muestra alertas si divs no estan vacios
    */


    if ($("#alertatoken").html().length > 0) {

        $("#magic-access").show();
        $("#regular-access").hide();
        $('#alertatoken').show();
        $("#btn_validar_credenciales").hide();
    }

    if ($("#alertarojapulsera").html().length > 0) {

        $("#magic-access").show();
        $("#regular-access").hide();
        $('#alertarojapulsera').show();
        $("#btn_validar_credenciales").hide();
    }


    if ($("#alertaclasica").html().length > 0) {

        $("#magic-access").hide();
        $("#regular-access").show();
        $('#alertaclasica').show();
        $("#btn_validar_credenciales").show();
    }

    if ($("#alertaroja").html().length > 0) {


        $("#magic-access").hide();
        $("#regular-access").show();
        $('#alertaroja').show();
        $("#btn_validar_credenciales").show();
    }
});


function toggle_access_mode() {
    /*
    limpio alertas
     */
    $("#alertaroja").html("");
    $('#alertausuario').html("");
    $('#alertaclave').html("");
    /*
    escondo alertas
     */
    $("#alertaroja").hide();
    $('#alertausuario').hide();
    $('#alertaclave').hide();

    if (estado_timer == false) {
        console.log('Prender.');

        $("#magic-access").toggle('slow');
        $("#regular-access").toggle('slow');
        //$("#btn_validar_credenciales").toggle();
        iniciar_timer();
    } else {
        console.log('Apagar.');
        $("#tipologueo").val('1');
        $("#magic-access").toggle('slow');
        $("#regular-access").toggle('slow');
        //$("#btn_validar_credenciales").toggle();
        apagar_timer();
    }
}

function iniciar_timer() {
    estado_timer = true;
    timer_token = setTimeout(function () {
        $("#campo_token").focus();
    }, 1000);
}

function apagar_timer() {
    estado_timer = false;
    clearTimeout(timer_token);
    $("#campo_usuario").focus();
}