var showToast = function(message, type, timeout){
    bootoast.toast({
        message: message,
        type: type,
        position: 'bottom-center',
        icon: '',
        timeout: timeout,
        animationDuration: 300,
        dismissible: true
    });
}

var showErrorToast = function(message){
    
    showToast(message, "danger", null);
}

var  showSuccessToast = function(message){
    
    showToast(message, "success", 2500);
}


